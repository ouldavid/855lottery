<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DefaultDataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['name'=>'Admin','username'=>'admin','email'=>'admin@855lottery.com','password'=>'$2y$10$RZVHfZY0mc5FeteMAUQOOuqcZx9dpJTaBpQDGfRXvtxb2cd/o0uXe','remember_token'=>'dbMs75aILLZRyUMP9RtKIxrwOzGta3gVoic3m60HDnmaTZBIZKdPjFUrVtae','location_id'=>1,'user_parent_id'=>1,'language'=>'en','is_activated'=>'1','created_at'=>now(),'updated_at'=>now()],
            ['name'=>'Master Phnom Penh','username'=>'master_pp','email'=>'master_pp@855lottery.com','password'=>'$2y$10$RZVHfZY0mc5FeteMAUQOOuqcZx9dpJTaBpQDGfRXvtxb2cd/o0uXe','remember_token'=>'dbMs75aILLZRyUMP9RtKIxrwOzGta3gVoic3m60HDnmaTZBIZKdPjFUrVtae','location_id'=>1,'language'=>'kh','user_parent_id'=>1,'is_activated'=>'1','created_at'=>now(),'updated_at'=>now()],
            ['name'=>'Dealer Phnom Penh','username'=>'dealer_pp','email'=>'dealer_pp@855lottery.com','password'=>'$2y$10$RZVHfZY0mc5FeteMAUQOOuqcZx9dpJTaBpQDGfRXvtxb2cd/o0uXe','remember_token'=>'dbMs75aILLZRyUMP9RtKIxrwOzGta3gVoic3m60HDnmaTZBIZKdPjFUrVtae','location_id'=>1,'language'=>'kh','user_parent_id'=>2,'is_activated'=>'1','created_at'=>now(),'updated_at'=>now()]
        ]);

        DB::table('user_settings')->insert([
            ['user_id'=>1,'discount'=>'75','bet_win'=>'70','bet_limit'=>'600000','digit'=>'2D','lottery_type_id'=>1,'created_at'=>now(),'updated_at'=>now()],
            ['user_id'=>1,'discount'=>'75','bet_win'=>'600','bet_limit'=>'100000','digit'=>'3D','lottery_type_id'=>1,'created_at'=>now(),'updated_at'=>now()],
            ['user_id'=>1,'discount'=>'75','bet_win'=>'4000','bet_limit'=>'10000','digit'=>'4D','lottery_type_id'=>1,'created_at'=>now(),'updated_at'=>now()],
            ['user_id'=>1,'discount'=>'75','bet_win'=>'70','bet_limit'=>'100000','digit'=>'2D','lottery_type_id'=>2,'created_at'=>now(),'updated_at'=>now()],
            ['user_id'=>1,'discount'=>'75','bet_win'=>'{"10":"500","9":"105","11":"100"}','bet_limit'=>'100000','digit'=>'3D','lottery_type_id'=>2,'created_at'=>now(),'updated_at'=>now()],
            ['user_id'=>2,'discount'=>'75','bet_win'=>'70','bet_limit'=>'600000','digit'=>'2D','lottery_type_id'=>1,'created_at'=>now(),'updated_at'=>now()],
            ['user_id'=>2,'discount'=>'75','bet_win'=>'600','bet_limit'=>'100000','digit'=>'3D','lottery_type_id'=>1,'created_at'=>now(),'updated_at'=>now()],
            ['user_id'=>2,'discount'=>'75','bet_win'=>'4000','bet_limit'=>'10000','digit'=>'4D','lottery_type_id'=>1,'created_at'=>now(),'updated_at'=>now()],
            ['user_id'=>2,'discount'=>'75','bet_win'=>'70','bet_limit'=>'100000','digit'=>'2D','lottery_type_id'=>2,'created_at'=>now(),'updated_at'=>now()],
            ['user_id'=>2,'discount'=>'75','bet_win'=>'{"10":"500","9":"105","11":"100"}','bet_limit'=>'100000','digit'=>'3D','lottery_type_id'=>2,'created_at'=>now(),'updated_at'=>now()],
            ['user_id'=>3,'discount'=>'75','bet_win'=>'70','bet_limit'=>'600000','digit'=>'2D','lottery_type_id'=>1,'created_at'=>now(),'updated_at'=>now()],
            ['user_id'=>3,'discount'=>'75','bet_win'=>'600','bet_limit'=>'100000','digit'=>'3D','lottery_type_id'=>1,'created_at'=>now(),'updated_at'=>now()],
            ['user_id'=>3,'discount'=>'75','bet_win'=>'4000','bet_limit'=>'10000','digit'=>'4D','lottery_type_id'=>1,'created_at'=>now(),'updated_at'=>now()],
            ['user_id'=>3,'discount'=>'75','bet_win'=>'70','bet_limit'=>'100000','digit'=>'2D','lottery_type_id'=>2,'created_at'=>now(),'updated_at'=>now()],
            ['user_id'=>3,'discount'=>'75','bet_win'=>'{"10":"500","9":"105","11":"100"}','bet_limit'=>'100000','digit'=>'3D','lottery_type_id'=>2,'created_at'=>now(),'updated_at'=>now()]
       ]);

        DB::table('roles')->insert([
            ['name'=>'Admin','description'=>'Can access everywhere','created_at'=>now(),'updated_at'=>now()],
            ['name'=>'Master','description'=>'','created_at'=>now(),'updated_at'=>now()],
            ['name'=>'Account','description'=>'Can read only','created_at'=>now(),'updated_at'=>now()],
            ['name'=>'Dealer','description'=>'','created_at'=>now(),'updated_at'=>now()]
        ]);

        DB::table('role_user')->insert([
            ['role_id'=>1,'user_id'=>1,'created_at'=>now(),'updated_at'=>now()],
            ['role_id'=>2,'user_id'=>2,'created_at'=>now(),'updated_at'=>now()],
            ['role_id'=>4,'user_id'=>3,'created_at'=>now(),'updated_at'=>now()]
        ]);

        DB::table('accounts')->insert([
            ['user_id'=>1,'balance'=>0,'created_at'=>now(),'updated_at'=>now()],
            ['user_id'=>2,'balance'=>100000000,'created_at'=>now(),'updated_at'=>now()],
            ['user_id'=>3,'balance'=>10000000,'created_at'=>now(),'updated_at'=>now()]
        ]);

        DB::table('currencies')->insert([
            ['currency'=>'USD','symbol'=>'$','created_at'=>now(),'updated_at'=>now()],
            ['currency'=>'KHR','symbol'=>'៛','created_at'=>now(),'updated_at'=>now()],
            ['currency'=>'VND','symbol'=>'₫','created_at'=>now(),'updated_at'=>now()],
            ['currency'=>'THB','symbol'=>'฿','created_at'=>now(),'updated_at'=>now()]
        ]);

        DB::table('settings')->insert([
            ['key'=>'result_source','name'=>'Result Source','value'=>'{"day":"https://www.minhngoc.net.vn/vedo/in-ve-do-truc-tiep.php?mien=1&page=1","night":"https://www.minhngoc.net.vn/vedo/in-ve-do-truc-tiep.php?mien=2&page=1"}','created_at'=>now(),'updated_at'=>now()],
            ['key'=>'result_release_time','name'=>'Result Release Time','value'=>'{"day":"16:45:00","night":"18:45:00"}','created_at'=>now(),'updated_at'=>now()],
            ['key'=>'bet_closing_time','name'=>'Closing Time','value'=>'{"day":"16:00:00","night":"18:00:00"}','created_at'=>now(),'updated_at'=>now()],
            ['key'=>'bet_setting','name'=>'Bet Setting','value'=>'[{"lottery_type_id":1,"setting":{"2D":{"bet_discount":"70","bet_win":"70","bet_limit":"600000"},"3D":{"bet_discount":"75","bet_win":"600","bet_limit":"100000"},"4D":{"bet_discount":"75","bet_win":"4000","bet_limit":"10000"}}},{"lottery_type_id":2,"setting":{"2D":{"bet_discount":"75","bet_win":"70","bet_limit":"100000"},"3D":{"bet_discount":"75","bet_win":{"10":"500","9":"105","11":"100"},"bet_limit":"100000"}}}]','created_at'=>now(),'updated_at'=>now()],
            ['key'=>'color','name'=>'Color','value'=>'["Black","Red","Lime","Blue","Yellow","Cyan","Magenta","Silver","Gray","Maroon","Olive","Green","Purple","Teal","Navy","White"]','created_at'=>now(),'updated_at'=>now()]
        ]);

        DB::table('shifts')->insert([
            ['shift'=>'Day','created_at'=>now(),'updated_at'=>now()],
            ['shift'=>'Night','created_at'=>now(),'updated_at'=>now()]
        ]);

        DB::table('weeklies')->insert([
            ['day'=>'Monday','created_at'=>now(),'updated_at'=>now()],
            ['day'=>'Tuesday','created_at'=>now(),'updated_at'=>now()],
            ['day'=>'Wednesday','created_at'=>now(),'updated_at'=>now()],
            ['day'=>'Thursday','created_at'=>now(),'updated_at'=>now()],
            ['day'=>'Friday','created_at'=>now(),'updated_at'=>now()],
            ['day'=>'Saturday','created_at'=>now(),'updated_at'=>now()],
            ['day'=>'Sunday','created_at'=>now(),'updated_at'=>now()]
        ]);

        DB::table('location_types')->insert([
            ['type'=>'Central','created_at'=>now(),'updated_at'=>now()],
            ['type'=>'South','created_at'=>now(),'updated_at'=>now()],
            ['type'=>'East','created_at'=>now(),'updated_at'=>now()]
        ]);

        DB::table('locations')->insert([
            ['location'=>'Phnom Penh','location_type_id'=>1,'currency_id'=>2,'created_at'=>now(),'updated_at'=>now()],
            ['location'=>'Chrey Thom','location_type_id'=>2,'currency_id'=>2,'created_at'=>now(),'updated_at'=>now()],
            ['location'=>'Bavet','location_type_id'=>3,'currency_id'=>3,'created_at'=>now(),'updated_at'=>now()],
            ['location'=>'Poipet','location_type_id'=>1,'currency_id'=>4,'created_at'=>now(),'updated_at'=>now()],
            ['location'=>'Pailin','location_type_id'=>1,'currency_id'=>4,'created_at'=>now(),'updated_at'=>now()],
            ['location'=>'Kratie','location_type_id'=>1,'currency_id'=>2,'created_at'=>now(),'updated_at'=>now()]
        ]);

        DB::table('posts')->insert([
            ['post'=>'A','lottery_type_id'=>1,'created_at'=>now(),'updated_at'=>now()],
            ['post'=>'B','lottery_type_id'=>1,'created_at'=>now(),'updated_at'=>now()],
            ['post'=>'C','lottery_type_id'=>1,'created_at'=>now(),'updated_at'=>now()],
            ['post'=>'D','lottery_type_id'=>1,'created_at'=>now(),'updated_at'=>now()],
            ['post'=>'LO4P','lottery_type_id'=>1,'created_at'=>now(),'updated_at'=>now()],
            ['post'=>'LOAB','lottery_type_id'=>1,'created_at'=>now(),'updated_at'=>now()],
            ['post'=>'LO7','lottery_type_id'=>1,'created_at'=>now(),'updated_at'=>now()],
            ['post'=>'លើ','lottery_type_id'=>2,'created_at'=>now(),'updated_at'=>now()],
            ['post'=>'ក្រោម','lottery_type_id'=>2,'created_at'=>now(),'updated_at'=>now()],
            ['post'=>'ត្រង់','lottery_type_id'=>2,'created_at'=>now(),'updated_at'=>now()],
            ['post'=>'ទត','lottery_type_id'=>2,'created_at'=>now(),'updated_at'=>now()]
        ]);

        DB::table('lottery_types')->insert([
            ['type'=>'vn','description'=>'Vietnamese Lottery','created_at'=>now(),'updated_at'=>now()],
            ['type'=>'th','description'=>'Thai Lottery','created_at'=>now(),'updated_at'=>now()]
        ]);

        DB::table('location_lottery_type')->insert([
            ['location_id'=>1,'lottery_type_id'=>1],
            ['location_id'=>2,'lottery_type_id'=>1],
            ['location_id'=>3,'lottery_type_id'=>1],
            ['location_id'=>4,'lottery_type_id'=>1],
            ['location_id'=>4,'lottery_type_id'=>2],
            ['location_id'=>5,'lottery_type_id'=>1],
            ['location_id'=>5,'lottery_type_id'=>2],
            ['location_id'=>6,'lottery_type_id'=>1]
        ]);


        DB::table('location_post')->insert([
            ['location_id'=>1,'post_id'=>1],
            ['location_id'=>1,'post_id'=>2],
            ['location_id'=>1,'post_id'=>3],
            ['location_id'=>1,'post_id'=>4],
            ['location_id'=>1,'post_id'=>5],
            ['location_id'=>1,'post_id'=>6],
            ['location_id'=>2,'post_id'=>1],
            ['location_id'=>2,'post_id'=>2],
            ['location_id'=>2,'post_id'=>6],
            ['location_id'=>3,'post_id'=>1],
            ['location_id'=>3,'post_id'=>2],
            ['location_id'=>3,'post_id'=>6],
            ['location_id'=>3,'post_id'=>7],
            ['location_id'=>4,'post_id'=>1],
            ['location_id'=>4,'post_id'=>2],
            ['location_id'=>4,'post_id'=>3],
            ['location_id'=>4,'post_id'=>4],
            ['location_id'=>4,'post_id'=>5],
            ['location_id'=>4,'post_id'=>6],
            ['location_id'=>5,'post_id'=>1],
            ['location_id'=>5,'post_id'=>2],
            ['location_id'=>5,'post_id'=>3],
            ['location_id'=>5,'post_id'=>4],
            ['location_id'=>5,'post_id'=>5],
            ['location_id'=>5,'post_id'=>6],
            ['location_id'=>6,'post_id'=>1],
            ['location_id'=>6,'post_id'=>2],
            ['location_id'=>6,'post_id'=>3],
            ['location_id'=>6,'post_id'=>4],
            ['location_id'=>6,'post_id'=>5],
            ['location_id'=>6,'post_id'=>6],

        ]);

        DB::table('post_settings')->insert([
            ['post_id'=>1,'shift_id'=>1,'setting'=>'[{"location_type_id":1,"value":{"2D":"1","3D":"1","4D":"1","setting":"1"}},{"location_type_id":2,"value":{"2D":"1","3D":"1","4D":"1","setting":"1"}},{"location_type_id":3,"value":{"2D":"1","3D":"1","4D":"1","setting":"1"}}]','created_at'=>now(),'updated_at'=>now()],
            ['post_id'=>2,'shift_id'=>1,'setting'=>'[{"location_type_id":1,"value":{"2D":"1","3D":"1","4D":"1","setting":"1"}},{"location_type_id":2,"value":{"2D":"1","3D":"1","4D":"1","setting":"1"}},{"location_type_id":3,"value":{"2D":"1","3D":"1","4D":"1","setting":"1"}}]','created_at'=>now(),'updated_at'=>now()],
            ['post_id'=>3,'shift_id'=>1,'setting'=>'[{"location_type_id":1,"value":{"2D":"1","3D":"1","4D":"0","setting":"1"}},{"location_type_id":2,"value":{"2D":"1","3D":"1","4D":"0","setting":"0"}},{"location_type_id":3,"value":{"2D":"1","3D":"1","4D":"0","setting":"0"}}]','created_at'=>now(),'updated_at'=>now()],
            ['post_id'=>4,'shift_id'=>1,'setting'=>'[{"location_type_id":1,"value":{"2D":"1","3D":"1","4D":"0","setting":"1"}},{"location_type_id":2,"value":{"2D":"1","3D":"1","4D":"0","setting":"0"}},{"location_type_id":3,"value":{"2D":"1","3D":"1","4D":"0","setting":"0"}}]','created_at'=>now(),'updated_at'=>now()],
            ['post_id'=>5,'shift_id'=>1,'setting'=>'[{"location_type_id":1,"value":{"2D":"23","3D":"19","4D":"16","setting":"1"}},{"location_type_id":2,"value":{"2D":"22","3D":"19","4D":"16","setting":"0"}},{"location_type_id":3,"value":{"2D":"22","3D":"19","4D":"16","setting":"0"}}]','created_at'=>now(),'updated_at'=>now()],
            ['post_id'=>6,'shift_id'=>1,'setting'=>'[{"location_type_id":1,"value":{"2D":"19","3D":"17","4D":"16","setting":"1"}},{"location_type_id":2,"value":{"2D":"18","3D":"17","4D":"16","setting":"1"}},{"location_type_id":3,"value":{"2D":"18","3D":"17","4D":"16","setting":"1"}}]','created_at'=>now(),'updated_at'=>now()],
            ['post_id'=>7,'shift_id'=>1,'setting'=>'[{"location_type_id":1,"value":{"2D":"0","3D":"7","4D":"0","setting":"0"}},{"location_type_id":2,"value":{"2D":"0","3D":"7","4D":"0","setting":"0"}},{"location_type_id":3,"value":{"2D":"0","3D":"7","4D":"0","setting":"1"}}]','created_at'=>now(),'updated_at'=>now()],
            ['post_id'=>1,'shift_id'=>2,'setting'=>'[{"location_type_id":1,"value":{"2D":"4","3D":"3","4D":"0","setting":"1"}},{"location_type_id":2,"value":{"2D":"4","3D":"3","4D":"0","setting":"1"}},{"location_type_id":3,"value":{"2D":"4","3D":"3","4D":"0","setting":"1"}}]','created_at'=>now(),'updated_at'=>now()],
            ['post_id'=>2,'shift_id'=>2,'setting'=>'[{"location_type_id":1,"value":{"2D":"1","3D":"1","4D":"1","setting":"1"}},{"location_type_id":2,"value":{"2D":"1","3D":"1","4D":"1","setting":"1"}},{"location_type_id":3,"value":{"2D":"1","3D":"1","4D":"1","setting":"1"}}]','created_at'=>now(),'updated_at'=>now()],
            ['post_id'=>3,'shift_id'=>2,'setting'=>'[{"location_type_id":1,"value":{"2D":"1","3D":"1","4D":"0","setting":"1"}},{"location_type_id":2,"value":{"2D":"1","3D":"1","4D":"0","setting":"0"}},{"location_type_id":3,"value":{"2D":"1","3D":"1","4D":"0","setting":"0"}}]','created_at'=>now(),'updated_at'=>now()],
            ['post_id'=>4,'shift_id'=>2,'setting'=>'[{"location_type_id":1,"value":{"2D":"1","3D":"1","4D":"0","setting":"1"}},{"location_type_id":2,"value":{"2D":"1","3D":"1","4D":"0","setting":"0"}},{"location_type_id":3,"value":{"2D":"1","3D":"1","4D":"0","setting":"0"}}]','created_at'=>now(),'updated_at'=>now()],
            ['post_id'=>5,'shift_id'=>2,'setting'=>'[{"location_type_id":1,"value":{"2D":"32","3D":"25","4D":"20","setting":"1"}},{"location_type_id":2,"value":{"2D":"31","3D":"25","4D":"20","setting":"0"}},{"location_type_id":3,"value":{"2D":"31","3D":"25","4D":"20","setting":"0"}}]','created_at'=>now(),'updated_at'=>now()],
            ['post_id'=>6,'shift_id'=>2,'setting'=>'[{"location_type_id":1,"value":{"2D":"28","3D":"23","4D":"20","setting":"1"}},{"location_type_id":2,"value":{"2D":"27","3D":"23","4D":"20","setting":"1"}},{"location_type_id":3,"value":{"2D":"27","3D":"23","4D":"20","setting":"1"}}]','created_at'=>now(),'updated_at'=>now()],
            ['post_id'=>7,'shift_id'=>2,'setting'=>'[{"location_type_id":1,"value":{"2D":"0","3D":"7","4D":"0","setting":"0"}},{"location_type_id":2,"value":{"2D":"0","3D":"7","4D":"0","setting":"0"}},{"location_type_id":3,"value":{"2D":"0","3D":"7","4D":"0","setting":"1"}}]','created_at'=>now(),'updated_at'=>now()],
            ['post_id'=>8,'shift_id'=>1,'setting'=>'[{"location_type_id":1,"value":{"2D":"1","3D":"0","4D":"0","setting":"1"}},{"location_type_id":2,"value":{"2D":"1","3D":"0","4D":"0","setting":"0"}},{"location_type_id":3,"value":{"2D":"1","3D":"0","4D":"0","setting":"0"}}]','created_at'=>now(),'updated_at'=>now()],
            ['post_id'=>9,'shift_id'=>1,'setting'=>'[{"location_type_id":1,"value":{"2D":"1","3D":"1","4D":"0","setting":"1"}},{"location_type_id":2,"value":{"2D":"1","3D":"1","4D":"0","setting":"0"}},{"location_type_id":3,"value":{"2D":"1","3D":"1","4D":"0","setting":"0"}}]','created_at'=>now(),'updated_at'=>now()],
            ['post_id'=>10,'shift_id'=>1,'setting'=>'[{"location_type_id":1,"value":{"2D":"0","3D":"1","4D":"0","setting":"1"}},{"location_type_id":2,"value":{"2D":"0","3D":"1","4D":"0","setting":"0"}},{"location_type_id":3,"value":{"2D":"0","3D":"1","4D":"0","setting":"0"}}]','created_at'=>now(),'updated_at'=>now()],
            ['post_id'=>11,'shift_id'=>1,'setting'=>'[{"location_type_id":1,"value":{"2D":"0","3D":"1","4D":"0","setting":"1"}},{"location_type_id":2,"value":{"2D":"0","3D":"1","4D":"0","setting":"0"}},{"location_type_id":3,"value":{"2D":"0","3D":"1","4D":"0","setting":"0"}}]','created_at'=>now(),'updated_at'=>now()]
        ]);

        DB::table('result_provinces')->insert([
            ['province'=>'TP. HCM','created_at'=>now(),'updated_at'=>now()],
            ['province'=>'Đồng Tháp','created_at'=>now(),'updated_at'=>now()],
            ['province'=>'Cà Mau','created_at'=>now(),'updated_at'=>now()],
            ['province'=>'Bến Tre','created_at'=>now(),'updated_at'=>now()],
            ['province'=>'Vũng Tàu','created_at'=>now(),'updated_at'=>now()],
            ['province'=>'Bạc Liêu','created_at'=>now(),'updated_at'=>now()],
            ['province'=>'Đồng Nai','created_at'=>now(),'updated_at'=>now()],
            ['province'=>'Cần Thơ','created_at'=>now(),'updated_at'=>now()],
            ['province'=>'Sóc Trăng','created_at'=>now(),'updated_at'=>now()],
            ['province'=>'Tây Ninh','created_at'=>now(),'updated_at'=>now()],
            ['province'=>'An Giang','created_at'=>now(),'updated_at'=>now()],
            ['province'=>'Bình Thuận','created_at'=>now(),'updated_at'=>now()],
            ['province'=>'Vĩnh Long','created_at'=>now(),'updated_at'=>now()],
            ['province'=>'Bình Dương','created_at'=>now(),'updated_at'=>now()],
            ['province'=>'Trà Vinh','created_at'=>now(),'updated_at'=>now()],
            ['province'=>'Long An','created_at'=>now(),'updated_at'=>now()],
            ['province'=>'Bình Phước','created_at'=>now(),'updated_at'=>now()],
            ['province'=>'Hậu Giang','created_at'=>now(),'updated_at'=>now()],
            ['province'=>'Tiền Giang','created_at'=>now(),'updated_at'=>now()],
            ['province'=>'Kiên Giang','created_at'=>now(),'updated_at'=>now()],
            ['province'=>'Đà Lạt','created_at'=>now(),'updated_at'=>now()],
            ['province'=>'Hà Nội','created_at'=>now(),'updated_at'=>now()]
        ]);

        DB::table('result_province_filters')->insert([
            ['location_type_id'=>1,'weekly_id'=>1,'result_province_id'=>1],
            ['location_type_id'=>1,'weekly_id'=>2,'result_province_id'=>4],
            ['location_type_id'=>1,'weekly_id'=>3,'result_province_id'=>7],
            ['location_type_id'=>1,'weekly_id'=>4,'result_province_id'=>10],
            ['location_type_id'=>1,'weekly_id'=>5,'result_province_id'=>13],
            ['location_type_id'=>1,'weekly_id'=>6,'result_province_id'=>1],
            ['location_type_id'=>1,'weekly_id'=>7,'result_province_id'=>19],
            ['location_type_id'=>2,'weekly_id'=>1,'result_province_id'=>2],
            ['location_type_id'=>2,'weekly_id'=>2,'result_province_id'=>6],
            ['location_type_id'=>2,'weekly_id'=>3,'result_province_id'=>8],
            ['location_type_id'=>2,'weekly_id'=>4,'result_province_id'=>11],
            ['location_type_id'=>2,'weekly_id'=>5,'result_province_id'=>13],
            ['location_type_id'=>2,'weekly_id'=>6,'result_province_id'=>16],
            ['location_type_id'=>2,'weekly_id'=>7,'result_province_id'=>20],
            ['location_type_id'=>3,'weekly_id'=>1,'result_province_id'=>1],
            ['location_type_id'=>3,'weekly_id'=>2,'result_province_id'=>4],
            ['location_type_id'=>3,'weekly_id'=>3,'result_province_id'=>7],
            ['location_type_id'=>3,'weekly_id'=>4,'result_province_id'=>10],
            ['location_type_id'=>3,'weekly_id'=>5,'result_province_id'=>13],
            ['location_type_id'=>3,'weekly_id'=>6,'result_province_id'=>1],
            ['location_type_id'=>3,'weekly_id'=>7,'result_province_id'=>19],
            ['location_type_id'=>3,'weekly_id'=>1,'result_province_id'=>2],
            ['location_type_id'=>3,'weekly_id'=>2,'result_province_id'=>5],
            ['location_type_id'=>3,'weekly_id'=>3,'result_province_id'=>8],
            ['location_type_id'=>3,'weekly_id'=>4,'result_province_id'=>11],
            ['location_type_id'=>3,'weekly_id'=>5,'result_province_id'=>14],
            ['location_type_id'=>3,'weekly_id'=>6,'result_province_id'=>16],
            ['location_type_id'=>3,'weekly_id'=>7,'result_province_id'=>20]
        ]);
    }
}
