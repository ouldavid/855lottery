<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bets', function(Blueprint $table)
		{
            $table->bigIncrements('id');
			$table->integer('user_id');
			$table->integer('shift_id');
            $table->integer('lottery_type_id');
			$table->integer('total')->comment('Total bet amount before discount');
            $table->integer('grand_total')->comment('Total bet amount after discount');
            $table->integer('currency_id');
			$table->integer('printed')->default(0);
            $table->integer('ticket');
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bets');
	}

}
