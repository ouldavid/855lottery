<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get username property.
     *
     * @return string
     */
    public function username()
    {
        $login = request()->input('login');
        $field = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        request()->merge([$field => $login]);

         return $field;
    }

    protected function authenticated(Request $request, $user)
    {
        if ($user->is_activated == 0) {
            // Log the user out.
            $this->logout($request);

            // Return them to the log in form.
            return redirect()->back()
                ->withInput($request->only($this->username(), 'remember'))
                ->withErrors([
                    // This is where we are providing the error message.
                    $this->username() => 'Your account is not active.',
                ]);
        } else {
            // Admin
            if($user->hasRole('Admin')) {
                return redirect()->route('dashboard');
            }else if($user->hasRole('Master')) {
                // Master
                return redirect()->route('user');
            }else if($user->hasRole('Account')) {
                // Account
                return redirect()->route('dashboard');
            }else {
                // Dealer
                return redirect()->route('home');
            }

        }
    }

   /* protected function authenticated(Request $request, $user)
    {
        if (Auth::check()) {
            return redirect()->route('dashboard');
        }

        if ($user->role_id == 1) {
            return redirect('/admin');
        } else if ($user->role_id == 2) {
            return redirect('/author');
        } else {
            return redirect('/blog');
        }

    }*/

}
