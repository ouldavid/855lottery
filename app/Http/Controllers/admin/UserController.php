<?php

namespace App\Http\Controllers\admin;

use App\Models\Account;
use App\Models\Location;
use App\Models\Role;
use App\Models\Setting;
use App\Models\UserSetting;
use App\Models\Transaction;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use mysql_xdevapi\Collection;
use Response;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // user access
        if(! auth()->user()->hasAnyRole(['Admin','Master'])) {
           abort(401, 'This action is unauthorized.');
        }
        // define variable
        if(auth()->user()->hasRole('Admin')) {
            // user admin
            $roles = Role::where('id', '!=', 1)->get();
        }else {
            // user master
            $roles = Role::whereNotIn('id', [1,2])->get();
        }
        $locations = Location::orderBy('location', 'ASC')->get();
        $parent_accounts = User::select('id', 'name')
            ->where('is_activated', '1')
            ->whereHas('roles', function ($query) {
                $query->select('roles.id', 'roles.name')->where('roles.id', 1);
            })->limit(1)->get();
        // load view
        return view('admin.user', compact(['roles', 'locations', 'parent_accounts']));
    }
    // insert user
    function store(Request $request)
    {
        if(auth()->user()->hasRole('Admin')) {
            // user is admin
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'username' => ['required', 'string', 'min:9', 'max:100', 'unique:users', 'regex:/^\S*$/u'],
                'password' => ['required', 'string', 'min:6', 'confirmed'],
                'slt_location' => ['required'],
                'slt_parent' => ['required'],
            ]);
        }else {
            // user is master
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'username' => ['required', 'string', 'min:9', 'max:100', 'unique:users', 'regex:/^\S*$/u'],
                'password' => ['required', 'string', 'min:6', 'confirmed'],
            ]);
        }
        // process validation
        if($validator->passes()) {
            // Store your data in database
            $user = new User();
            $user->name = ucwords($request->name);
            $user->username = strtolower($request->username);
            $user->password = Hash::make($request->password);
            $user->location_id = (auth()->user()->hasRole('Master')) ? auth()->user()->location_id : $request->slt_location;
            $user->user_parent_id = (auth()->user()->hasRole('Master')) ? auth()->user()->id : $request->slt_parent;
            $user->is_activated = $request->is_activated;
            $user->save();
            // role,user
            $user_last_id = $user->id;
            $role_user = User::find($user_last_id);
            $role_user->roles()->attach($request->slt_role);
            // account
            $account = new Account();
            $account->user_id = $user_last_id;
            $account->balance = 0;
            $account->save();
            // user setting
            //$user_settings = UserSetting::where('user_id', 0)->get();
            $settings= json_decode(Setting::where('key','bet_setting')->first()->value);
            foreach ($settings as $setting) {
                foreach ($setting->setting as $key=>$user_setting){
                    $data_setting[] = [
                        'user_id' => $user_last_id,
                        'discount' => $user_setting->bet_discount,
                        'bet_win' => gettype($user_setting->bet_win) == 'object' ? json_encode($user_setting->bet_win): $user_setting->bet_win,
                        'bet_limit' => $user_setting->bet_limit,
                        'digit' => $key,
                        'lottery_type_id' => $setting->lottery_type_id,
                        'created_at' => now(),
                        'updated_at' => now(),
                    ];
                }
            }
            //dd($data_setting);
            UserSetting::insert($data_setting);
            // message success
            $request->session()->flash('flash_success');
            return Response::json(['success' => '1']);
        }
        // message error
        return Response::json(['errors' => $validator->errors()]);

    }
    // show list user
    function show(Request $request)
    {

        $columns = array(
        0 =>'id',
        1 =>'name',
        2=> 'username',
            3=> 'roles.name',
    );

        if(auth()->user()->hasRole('Admin')) {
            // Admin
            $totalData = User::count();
        }else {
            // Master
            $totalData = User::where('user_parent_id',auth()->user()->id)->count();
        }

        $totalFiltered = $totalData;
        $users = User::get();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $sort = ($dir == 'asc') ? 'sortBy' : 'sortByDesc';

        if(empty($request->input('search.value')))
        {
            $users ->$sort($order);
        }
        else {
            $search = $request->input('search.value');

            $users =  User::where('name','LIKE',"%{$search}%")
                ->orWhere('username', 'LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)->get();

            if(auth()->user()->hasRole('Admin')) {
                // Admin
                $totalFiltered = User::where('name','LIKE',"%{$search}%")
                    ->orWhere('username', 'LIKE',"%{$search}%")
                    ->count();
            }else {
                // Master
                $totalFiltered = User::where('user_parent_id',auth()->user()->id)
                    ->where('name','LIKE',"%{$search}%")
                    ->orWhere('username', 'LIKE',"%{$search}%")
                    ->count();
            }
        }

        $data = array();
        if(!empty($users))
        {
            if(!auth()->user()->hasRole('Admin')) {
                $users = $users->where('user_parent_id',auth()->user()->id);
            }else {
                // select except admin
                $users = $users->where('id', '!=', 1);
            }

            foreach ($users as $key=>$user)
            {
                // data id modal
                $data_edit = ' "id":'.$user->id.',"name":"'.$user->name.'","username":"'.$user->username.'","user_role":"'.$user->roles[0]->id.'","location":"'.$user->location_id.'","user_parent":"'.$user->user_parent_id.'","is_activated":"'.$user->is_activated.'" ';
                $data_setting = ' "id":'.$user->id.' ';
                $data_deposit = ' "id":'.$user->id.',"name":"'.$user->name.'" ';
                $data_withdraw = ' "id":'.$user->id.',"name":"'.$user->name.'" ';

                // array data
                $nestedData['id'] = $key+1;
                $nestedData['name'] = $user->name;
                $nestedData['username'] = $user->username;
                $nestedData['roles'] = !empty($user->roles[0]->name) ? $user->roles[0]->name : "-";
                $nestedData['is_activated'] = $user->is_activated=='1' ? '<button class="btn btn-xs btn-success">Active</button>' : '<button class="btn btn-xs btn-warning">Inactive</button>';
                $nestedData['balance'] = (!empty($user->account)) ? $user->account->balance : '-';

                if(auth()->user()->hasRole('Admin')) {
                    if($user->roles[0]->id==2) {
                        // Master
                        if($user->is_activated=='1') {
                            $nestedData['setting'] = "<a href='javascript:void(0)' class='btn btn-xs btn-default data-deposit' data-target='#myModal-deposit' data-id='{ $data_deposit }' data-toggle='modal'>Deposit</a>
                            <a href='javascript:void(0)' class='btn btn-xs btn-default data-withdraw' data-target='#myModal-withdraw' data-id='{ $data_withdraw }' data-toggle='modal'>Withdraw</a>
                            <a href='javascript:void(0)' class='btn btn-xs btn-default data-setting' data-target='#myModal-setting' data-id='{ $data_setting }' data-toggle='modal'><i class='fas fa-user-cog'></i> User Setting</a>
                            <a href='javascript:void(0)' class='btn btn-xs btn-default data-password' onclick='reset_password($user->id,\"$user->name\")'><i class='fas fa-unlock-alt'></i> Reset Password</a>";
                        }else {
                            $nestedData['setting'] = "<a href='javascript:void(0)' class='btn btn-xs btn-default data-setting' data-target='#myModal-setting' data-id='{ $data_setting }' data-toggle='modal'><i class='fas fa-user-cog'></i> User Setting</a>
                            <a href='javascript:void(0)' class='btn btn-xs btn-default data-password' onclick='reset_password($user->id,\"$user->name\")'><i class='fas fa-unlock-alt'></i> Reset Password</a>";
                        }
                    }else if($user->roles[0]->id==4) {
                        // Dealer
                        $nestedData['setting'] = "<a href='javascript:void(0)' class='btn btn-xs btn-default data-setting' data-target='#myModal-setting' data-id='{ $data_setting }' data-toggle='modal'><i class='fas fa-user-cog'></i> User Setting</a>
                        <a href='javascript:void(0)' class='btn btn-xs btn-default data-password' onclick='reset_password($user->id,\"$user->name\")'><i class='fas fa-unlock-alt'></i> Reset Password</a>";
                    }else {
                        $nestedData['setting'] = "-";
                    }
                }else {
                    if($user->is_activated=='1') {
                        $nestedData['setting'] = "<a href='javascript:void(0)' class='btn btn-xs btn-default data-deposit' data-target='#myModal-deposit' data-id='{ $data_deposit }' data-toggle='modal'>Deposit</a>
                            <a href='javascript:void(0)' class='btn btn-xs btn-default data-withdraw' data-target='#myModal-withdraw' data-id='{ $data_withdraw }' data-toggle='modal'>Withdraw</a>
                            <a href='javascript:void(0)' class='btn btn-xs btn-default data-setting' data-target='#myModal-setting' data-id='{ $data_setting }' data-toggle='modal'><i class='fas fa-user-cog'></i> User Setting</a>
                            <a href='javascript:void(0)' class='btn btn-xs btn-default data-password' onclick='reset_password($user->id,\"$user->name\")'><i class='fas fa-unlock-alt'></i> Reset Password</a>";
                    }else {
                        $nestedData['setting'] = "<a href='javascript:void(0)' class='btn btn-xs btn-default data-setting' data-target='#myModal-setting' data-id='{ $data_setting }' data-toggle='modal'><i class='fas fa-user-cog'></i> User Setting</a>
                        <a href='javascript:void(0)' class='btn btn-xs btn-default data-password' onclick='reset_password($user->id,\"$user->name\")'><i class='fas fa-unlock-alt'></i> Reset Password</a>";
                    }
                }

                $nestedData['actions'] = ($user->roles[0]->id==1) ? "-" : "<a href='javascript:void(0)' class='btn btn-xs btn-default data-edit' data-target='#myModal' data-id='{ $data_edit }' data-toggle='modal' onclick='edit_data()'><i class='fa fa-edit'></i> Edit</a>
<a href='javascript:void(0)' class='btn btn-xs btn-default' onclick='delete_data($user->id,\"$user->name\")'><i class='fa fa-trash'></i> Delete</a>";
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);

    }
    // update user
    public function update(Request $request, $id)
    {
        if(auth()->user()->hasRole('Admin')) {
            // user is admin
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'username' => ['required', 'string', 'min:9', 'max:100', 'regex:/^\S*$/u', Rule::unique('users')->ignore($id)],
                'password' => ['sometimes', 'nullable', 'string', 'min:6', 'confirmed'],
                'slt_location' => ['required'],
                'slt_parent' => ['required'],
            ]);
        }else {
            // user is master
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'username' => ['required', 'string', 'min:9', 'max:100', 'regex:/^\S*$/u', Rule::unique('users')->ignore($id)],
                'password' => ['sometimes', 'nullable', 'string', 'min:6', 'confirmed']
            ]);
        }

        if($validator->passes()) {
            // Store your data in database
            $user = User::find($id);
            $user->name = ucwords($request->name);
            $user->username = strtolower($request->username);
            if(!empty($request->password)) {
                $user->password = Hash::make($request->password);
            }
            $user->location_id = (auth()->user()->hasRole('Master')) ? auth()->user()->location_id : $request->slt_location;
            $user->user_parent_id = (auth()->user()->hasRole('Master')) ? auth()->user()->id : $request->slt_parent;
            $user->is_activated = $request->is_activated;
            // check data update change or not
            if ($user->isDirty() OR ($user->roles[0]->id != $request->slt_role)) {
                // save update user
                $user->save();
                // update pivot role,user
                if(!empty($request->slt_role)) {
                    $user->roles()->sync($request->slt_role);
                }
                // alert message
                $request->session()->flash('flash_success');
                return Response::json(['success' => '1']);
            } else {
                // alert message
                $request->session()->flash('flash_warning');
                return Response::json(['warning' => '1']);
            }

        }

        return Response::json(['errors' => $validator->errors()]);
    }
    // update user setting
    public function updateSetting(Request $request)
    {
        $data = $request->all();
        for($i = 0; $i < count($data['id']); $i++) {
            $bet_win = $data['bet_win'][$i];
            if(gettype($data['bet_win'][$i])=='array'){
                $bet_win = json_encode($data['bet_win'][$i]);
            }
            $user_setting = UserSetting::find($data['id'][$i]);
            $user_setting->discount = $data['discount'][$i];
            $user_setting->bet_win = $bet_win;
            $user_setting->bet_limit = $data['bet_limit'][$i];
            $user_setting->save();
        }
        // alert message
        $request->session()->flash('flash_success');
        return  redirect()->back();
    }
    // delete data by id
    public function destroy(Request $request, $id)
    {
        // delete user
        $user = User::find($id);
        $user->roles()->detach();
        $user->delete();
        // delete user setting
        UserSetting::where('user_id',$id)->delete();
        // alert message
        $request->session()->flash('flash_danger');
        return Response::json(['success' => '1']);
    }
    // reset password
    public function resetPassword(Request $request, $id)
    {
        // check parents user
        $this->user_parents($id);
        if(in_array($id, $this->user_parents)) {
            if(auth()->user()->hasAnyRole(['Admin','Master'])) {
                $default_password = '123456789';
                User::find($id)->update(['password'=> Hash::make($default_password)]);
                // alert message
                $request->session()->flash('flash_success');
                return Response::json(['success' => '1']);
            }
        }else {
            return Response::json(['warning' => '1']);
        }
    }

    // ajax user setting
    public function ajaxSetting(Request $request, $id)
    {
        if($request->ajax()) {
            $user_settings = UserSetting::where('user_id', $id)->orderBy('digit','ASC')->get();
            // view ajax
            return view('admin.ajax_user_setting', compact(['user_settings']));
        }
    }
    // ajax parent user
    public function ajaxParent(Request $request)
    {
        if($request->ajax()) {
            if($request->role_id) {
                $role_id = $request->role_id;
                if ($role_id == 2) {
                    // Master
                    $role_ids = [1];
                } else if ($role_id == 3) {
                    // Account
                    $role_ids = [1, 2];
                } else if ($role_id == 4) {
                    // Dealer
                    $role_ids = [2];
                }
            }
            // collection all user active
            $users = User::all();
            //$users = User::where('is_activated', '1')->get();
            $locations = Location::orderBy('location', 'ASC')->get();
            if($request->edit_user) {
                $html_role = false;
                // user update
                foreach ($users as $user) {
                    // user role
                    foreach ($user->roles->whereIn('id', $role_ids) as $role) {
                        $html_parents[] = '<option value="' . $user->id . '">' . $user->name . " (" . $role->name . ")" . '</option>';
                    }
                }
                if($request->role_id==2) {
                    // Check master role have parents or not
                    $check_parents = User::where('user_parent_id', $request->user_id)->count();
                    if($check_parents>0) {
                        $html_role = true;
                    }
                    foreach ($locations as $location)
                        $html_location[] = '<option value="' . $location->id . '">' . $location->location . '</option>';
                }else {
                    foreach ($users->where('id', $request->user_id) as $user_location)
                        $html_location[] = '<option value="' . $user_location->location->id . '">' . $user_location->location->location . '</option>';
                }
                return response()->json(['user_parents' => $html_parents, 'location' => $html_location, 'user_role' => $html_role]);

            } else if($request->role_id && $request->parents_id) {
                // location depend on user role
                $html_location[] = '<option value="" selected="">-- no select --</option>';
                if($request->role_id==2) {
                    // Master
                    foreach ($locations as $location)
                        $html_location[] = '<option value="' . $location->id . '">' . $location->location . '</option>';
                }else {
                    foreach ($users->where('id', $request->parents_id) as $user_location)
                        $html_location[] = '<option value="' . $user_location->location->id . '">' . $user_location->location->location . '</option>';
                }
                return response()->json(['location' => $html_location]);
            }else {
                // user parents
                $html_parents[] = '<option value="" selected="">-- no select --</option>';
                if($request->user_id) {
                    // Master change to others
                    foreach ($users->whereNotIn('id', $request->user_id) as $user) {
                        // user role
                        foreach ($user->roles->whereIn('id', $role_ids) as $role) {
                            $html_parents[] = '<option value="' . $user->id . '">' . $user->name . " (" . $role->name . ")" . '</option>';
                        }
                    }
                }else {
                    foreach ($users as $user) {
                        // user role
                        foreach ($user->roles->whereIn('id', $role_ids) as $role) {
                            $html_parents[] = '<option value="' . $user->id . '">' . $user->name . " (" . $role->name . ")" . '</option>';
                        }
                    }
                }
                return response()->json(['user_parents' => $html_parents]);
            }

        }
    }
    // user balance
    public function deposit(Request $request, $id)
    {
        // function balance user parent
        $this->user_parents($id);
        // check parents user
        if(in_array($id, $this->user_parents)) {
            if(auth()->user()->hasRole('Admin')) {
                $validator = Validator::make($request->all(), [
                    'deposit' => 'required|min:1|numeric',
                ]);
            }else {
                $validator = Validator::make($request->all(), [
                    'deposit' => 'required|max:'.auth()->user()->account->balance.'|min:1|numeric',
                ]);
            }
            // if deposit submit
            if($validator->passes()) {
                if($request->deposit) {
                    $account = Account::find($this->account_id);
                    $account->user_id = $id;
                    $account->balance = ($this->old_balance+$request->deposit);
                    $account->save();
                    // transaction
                    $data_transaction[] = [
                        'account_id' => $account->id,
                        'amount' => $request->deposit,
                        'action' => 1,
                        'last_balance' => $this->old_balance,
                        'noted' => (!empty($request->remark)) ? $request->remark : '',
                        'created_at' => now(),
                        'updated_at' => now(),
                    ];
                    Transaction::insert($data_transaction);
                    // user account Master
                    if(auth()->user()->hasRole('Master')) {
                        $account_parent = Account::find(auth()->user()->account->id);
                        $account_parent->user_id = auth()->user()->id;
                        $account_parent->balance = (auth()->user()->account->balance-$request->deposit);
                        $account_parent->save();
                        // transaction
                        /*$data_transaction_parent[] = [
                            'account_id' => auth()->user()->account->id,
                            'amount' => $request->deposit,
                            'action' => 'w',
                            'last_balance' => auth()->user()->account->balance,
                            'noted' => (!empty($request->remark)) ? $request->remark : '',
                            'created_at' => now(),
                            'updated_at' => now(),
                        ];
                        Transaction::insert($data_transaction_parent);*/
                    }
                }
                // alert message
                $request->session()->flash('flash_success');
                return Response::json(['success' => '1']);
            }
            return Response::json(['errors' => $validator->errors()]);
        }else {
            abort(401, 'This action is unauthorized.');
        }

    }
    public function withdraw(Request $request, $id)
    {
        // function balance user parent
        $this->user_parents($id);
        // check parents user
        if(in_array($id, $this->user_parents)) {
            $validator = Validator::make($request->all(), [
                'withdraw' => 'required|max:'.$this->old_balance.'|min:1|numeric',
            ]);
            if($validator->passes()) {
                if($request->withdraw) {
                    $account = Account::find($this->account_id);
                    $account->user_id = $id;
                    $account->balance = ($this->old_balance-$request->withdraw);
                    $account->save();
                    // transaction
                    $data_transaction[] = [
                        'account_id' => $account->id,
                        'amount' => $request->withdraw,
                        'action' => 2,
                        'last_balance' => $this->old_balance,
                        'noted' => (!empty($request->remark)) ? $request->remark : '',
                        'created_at' => now(),
                        'updated_at' => now(),
                    ];
                    Transaction::insert($data_transaction);
                    // user account Master
                    if(auth()->user()->hasRole('Master')) {
                        $account_parent = Account::find(auth()->user()->account->id);
                        $account_parent->user_id = auth()->user()->id;
                        $account_parent->balance = (auth()->user()->account->balance+$request->withdraw);
                        $account_parent->save();
                        // transaction
                        /*$data_transaction_parent[] = [
                            'account_id' => auth()->user()->account->id,
                            'amount' => $request->withdraw,
                            'action' => 'd',
                            'last_balance' => auth()->user()->account->balance,
                            'noted' => (!empty($request->remark)) ? $request->remark : '',
                            'created_at' => now(),
                            'updated_at' => now(),
                        ];
                        Transaction::insert($data_transaction_parent);*/
                    }
                    // alert message
                    $request->session()->flash('flash_success');
                    return Response::json(['success' => '1']);
                }
            }
            return Response::json(['errors' => $validator->errors()]);
        }else {
            abort(401, 'This action is unauthorized.');
        }

    }
    // balance user parent
    function user_parents($id) {
        $users = User::where('user_parent_id', auth()->user()->id)
            ->where('is_activated', '1')
            ->where('id', '!=', 1)
            ->get(['id']);
        foreach ($users as $user) {
            if($user->id == $id) {
                $this->account_id = $user->account->id;
                $this->old_balance = $user->account->balance;
            }
            $this->user_parents[] = $user->id;
        }
    }

}
