<?php

namespace App\Http\Controllers\admin;

use App\Models\Currency;
use App\Models\Location;
use App\Models\PostSetting;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $locations= Location::get();
        $currencies = Currency::get();
        return view('admin.setting',compact('locations','currencies'));
    }
    function currencySetting(Request $request){
        //dd($request->items);
        Location::where('id', $request->location_id)
            ->update(['currency_id' => $request->currency_id]);
    }
}
