<?php

namespace App\Http\Controllers\admin;

use App\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $profile = auth()->user();
        // load page
        return view('admin.profile', compact('profile'));
    }

    public function update(Request $request)
    {
        $id = auth()->user()->id;
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'min:9', 'max:100', 'regex:/^\S*$/u', Rule::unique('users')->ignore($id)]
        ]);
        if($validator->passes()) {
            $user = User::find($id);
            $user->name = ucwords($request->name);
            $user->username = strtolower($request->username);
            $user->save();
            // alert message
            $request->session()->flash('flash_success');
            return  redirect()->back();
        }
        // message error
        return redirect()->back()->withErrors($validator->errors());
    }

    // Change Password
    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_password' => ['required', new MatchOldPassword],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
        if($validator->passes()) {
            User::find(auth()->user()->id)->update(['password'=> Hash::make($request->password)]);
            // alert message
            $request->session()->flash('flash_success');
            return  redirect()->back();
        } else {
            // message error
            return redirect()->back()->withErrors($validator->errors())->with('activeTab',$request->hidden_activeTab);
        }

    }

}
