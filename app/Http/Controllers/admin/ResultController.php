<?php

namespace App\Http\Controllers\admin;

use App\Models\Bet;
use App\Models\Location;
use App\Models\LocationType;
use App\Models\Result;
use App\Models\ResultFilter;
use App\Models\ResultProvince;
use App\Models\ResultProvinceFilter;
use App\Models\Shift;
use App\Models\Winner;
use App\Models\Setting;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DOMDocument;
use Illuminate\Support\Collection;

class ResultController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(){
        return view('admin.result');
    }

    function show(Request $request){
        $locations=null; $data=null; $msg=null; $data_result=null;
        $admin= auth()->user()->hasRole('Admin');
        if($admin) {
            $locations = LocationType::get();
        }
        else{
            $locations= LocationType::where('id',1)->orwhere('id',auth()->user()->location->location_type->id)->get();
        }
        //Get Last 3 Results from DB
        $results = Result::whereDate('created_at', $request->date)->take(4)->get();

        //Get Result by Location type and if not yet release result will get the latest one
        $result_filters = ResultFilter::whereDate('created_at', $request->date)->get();
        if(count($result_filters) > 0) {
            $data_result = $this->getDataResultHTML($locations, $results, $result_filters,$admin);
        }
        else{
            $msg=
                '<div class="box">
            <div class="box-body"><br/>
                <code class="center-block"><h4>Have no Result in the Database yet, so please get the first Result to the system.</h4></code><br/>
            </div>
        </div>';
        }
        $data=[
            'results'=> $data_result,
            'msg' => $msg
        ];
        return response()->json($data);
    }
    function getDataResultHTML($locations,$results,$result_filters,$admin_role){
        $data_result =
            '<ul class="nav nav-tabs" role="tablist" id="location">
                <li class="nav-item active">
                    <a class="nav-link" id="all-tab-link" data-toggle="tab" href="#all-tab"
                    role="tab" aria-controls="all-tab-link" aria-selected="true">All Results</a>
                </li>';
        foreach($locations as $location){
            $data_result .=
                '<li class="nav-item">
                    <a class="nav-link" id="tab-link-'.$location->id.'" data-toggle="tab" href="#tab-'.$location->id.'"
                       role="tab" aria-controls="tab-link-'.$location->id.'" aria-selected="true">'.(($location->id == 1) ? 'Night' : ($admin_role ? $location->type: 'Noon')).'</a>
                </li>';
        }

        $data_result .=
            '</ul>
            <div class="tab-content card">
                <div class="row tab-pane active" id="all-tab" role="tabpanel" aria-labelledby="all-tab">';
        foreach($results as $key=>$result){
            $i=1; $p=''; $arrays=json_decode($result->result);
            $data_result .=
                '<div class="col-md-3 col-sm-3 col-xs-12">
                                <div class="box">
                                    <div class="box-body">
                                        <h3>'.$result->result_province->province.'</h3>
                                        <table id="table-result" class="table table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th>Post</th>
                                                <th>Result</th>
                                            </tr>
                                            </thead>
                                            <tbody>';
            foreach($arrays as $data){
                if($key < $results->count()-1){
                    if($i==1 or $i==2) $p="A";
                    elseif($i==count($arrays)) $p="BCD";
                    elseif($i==3) $p="LO";
                    else $p='';
                }
                else{
                    if($i==1) $p="BCD";
                    elseif($i==2) $p="LO";
                    elseif($i==count($arrays) or $i==(count($arrays)-1)) $p="A";
                    else $p='';
                }

                $data_result.=
                    '<tr>
                                                        <td>'.$p.'</td>
                                                        <td>'.$data.'</td>
                                                    </tr>';
                $i++;
            }

            $data_result.='
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>';
        }

        $data_result .=
            '</div>';
        foreach($locations as $location) {
            $data_result .=
                '<div class="row tab-pane fade" id = "tab-'.$location->id.'" role = "tabpanel" aria-labelledby = "tab-'.$location->id.'" >';
            foreach ($result_filters->where('location_type_id', '=', $location->id)->unique('result_id') as $result) {
                $data_result .= '
                        <div class="col-sm-4">
                            <div class="box">
                                <div class="box-body">
                                    <h3>'.$result->result->result_province->province.'</h3>
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th>Post</th>
                                                <th> 2D </th>
                                                <th> 3D </th>
                                            </tr>
                                            </thead>
                                            <tbody>';
                foreach ($result_filters->where('location_type_id', '=', $location->id)->where('result_id',$result->result_id) as $result_filter) {
                    $data_result .=
                        '<tr>
                                                        <td>' . $result_filter->post->post . '</td>
                                                        <td>' . ($result_filter->{"2D"}) . '</td>
                                                        <td>' . ($result_filter->{"3D"}) . '</td>
                                                    </tr>';
                }
                $data_result .=
                    '</tbody >
                                        </table >
                                </div >
                            </div >
                        </div >';
            }
            $data_result .=
                '</div >';
        }
        $data_result .='</div>';

        return $data_result;
    }

    /******* Get Result ******/
    function getResult(){
        date_default_timezone_set(env('APP_TIMEZONE'));
        $settings = Setting::all();
        $result_release_time = json_decode($settings->where('key',  'result_release_time')->first()->value);
        $result_source = json_decode($settings->where('key',  'result_source')->first()->value);

        //If Time is NOON_RESULT_TIME(16:45:00) < Current time
        $day = strtotime($result_release_time->day) < time() ? 0 : -1;

        //If Result Filter today not exist
        if(!(ResultFilter::whereDate('created_at', '=', Carbon::today()->addDay($day)->toDateString())->exists())) {
            $date = $this->getDayResult($result_source->day);

            //Insert Result by province to DB
            foreach (LocationType::where('id','!=',1)->latest()->get() as $location){
                $this->getDayResultFilter($location->id, date('N', strtotime($day.' days')),$date);
            }

            /** Get Result Filters Function **/
            $msg = 'This is the updated Noon Result of Today';
        }
        else{
            $msg='This is the latest updated of today Noon Result';
        }

        $day = strtotime($result_release_time->night) < time() ? 0 : -1;

        if(!(ResultFilter::whereHas('result', function($item){
            $item->where('shift_id',  2);
        })
            ->whereDate('created_at', '=', Carbon::today()->addDay($day)
                ->toDateString())
            ->exists())) {

            $date = $this->getNightResults($result_source->night);
            $this->getNightResultFilter(date('N', strtotime($day.' days')), $date);

            $msg='This is the latest updated of all today Result';

        }

        return response()->json(array('msg'=> $msg), 200);
    }
    function getHtmlTableResult($url){
        $html = file_get_contents($url);

        /*** a new dom object ***/
        $dom = new domDocument;

        /*** load the html into the object ***/
        @$dom->loadHTML($html);

        /*** the table by its tag name ***/
        $tables = $dom->getElementsByTagName('tbody');

        return $tables;
    }
    function getDateResult($tables){
        /* Get date of result */
        $row = $tables->item(1)->getElementsByTagName('tr');
        $col = $row[0]->getElementsByTagName('td')->item(1)->nodeValue;
        $date = substr($col, -4) . '-' . substr($col, 3, 2) . '-' . substr($col, 0, 2) . ' ' . date("H:i:s");
        return $date;
    }
    function getDayResult($url){
        $tables = $this->getHtmlTableResult($url);
        $date = $this->getDateResult($tables);
        $results = array();
        for ($i = 0; $i < 3; $i++) {
            /*** loop over the table rows ***/
            foreach ($tables->item($i + 4)->getElementsByTagName('tr') as $key => $row) {

                /*** get each column by tag name ***/
                $cols = $row->getElementsByTagName('td');
                $str = trim($cols->item(0)->nodeValue);
                $n = strlen($str);
                if ($key == 0) $n = 1;

                switch ($n) {
                    case 10:
                    case 35:
                        $data = str_split($str, 5);
                        foreach ($data as $d) {
                            $results[$i][] = $d;
                        }
                        break;
                    case 12:
                        $data = str_split($str, 4);
                        foreach ($data as $d) {
                            $results[$i][] = $d;
                        }
                        break;
                    default:
                        $results[$i][] = $str;
                }
            }
        }

        //Insert Result to DB
        foreach ($results as $result) {
            $province = ResultProvince::where('province', $result[0])->first();

            //Remove Province and Province Code
            unset($result[0], $result[1]);
            //Reset Array Index
            $result = array_values($result);

            $data = Result::firstOrNew(['result' => json_encode($result)], ['result_province_id' => $province->id, 'shift_id'=> 1,'lottery_type_id' => 1]);
            $data->created_at = $date;
            $data->save();
        }

        return $date;
    }
    function getDayResultFilter($loc_type_id, $day, $date){
        $provinces= ResultProvinceFilter::where([
            'location_type_id'=> $loc_type_id,
            'weekly_id'=> $day
        ])->get();

        //dd($provinces);
        foreach ($provinces as $province){
            $result= Result::where('result_province_id','=',$province->result_province_id)->first();
            $results= json_decode($result->result);
            $post=5; /* 5 posts */

            for($i=1;$i<=$post;$i++){
                if($i!=1){
                    $data = new ResultFilter();
                    $data->result_id= $result->id;
                    $data->post_id = $i;
                    $data->location_type_id= $loc_type_id;
                    $data->created_at= $date;
                }

                switch ($i){
                    case 1:
                        for($x=0;$x<count($results)-3;$x++){
                            $data = new ResultFilter();
                            $data->result_id= $result->id;
                            $data->post_id = $i;
                            $data->location_type_id= $loc_type_id;
                            $data->{'2D'} = substr($results[$x+2], -2);
                            $data->{'3D'} = substr($results[$x+2], -3);
                            $data->created_at= $date;
                            $data->save();
                        }
                        break;

                    case 2:
                        $data->{'2D'} = $results[0];
                        $data->{'3D'} = $results[1];
                        $data->save();
                        break;

                    case 3:
                        switch ($loc_type_id){
                            case 2:
                                $data->{'2D'} = substr(end($results), 1, -3);
                                $data->{'3D'} = substr(end($results), -3);
                                $data->save();
                                break;

                            case 3:
                            case 4:
                                $data->{'2D'} = substr(end($results), -2);
                                $data->{'3D'} = substr(end($results), -3);
                                $data->save();
                                break;
                        }
                        break;

                    case 4:
                        $data->{'2D'} = substr(end($results), -2);
                        $data->{'3D'} = substr(end($results), 1, -2);
                        $data->save();
                        break;

                    case 5:
                        $data->{'2D'} = substr(end($results), 1, -4).substr(end($results), -1);
                        $data->{'3D'} = substr(end($results), 2, -1);
                        $data->save();
                        break;
                }
            }
        }
    }
    function getNightResults($url){
        $date_block = explode('<span class="txtngay">' , file_get_contents($url));
        $date_data= (explode('</span>',$date_block[1]));
        $date = date_format(date_create_from_format("j/m/Y",trim($date_data[0])),"Y-m-d").' '. date("H:i:s");

        $tables = $this->getHtmlTableResult($url);
        $string = trim(preg_replace("/[^A-Za-z0-9]/", ' ', $tables->item(2)->nodeValue));
        $results = explode(' ',preg_replace('/\s+/', ' ', $string));
        foreach ($results as $key=>$result){
            switch ($key){
                case 2:
                case 3:
                    $results[$key] = wordwrap($result , 5 , ' ' , true );
                    break;
                case 4:
                case 5:
                    $results[$key] = wordwrap($result , 4 , ' ' , true );
                    break;
                case 6:
                    $results[$key] = wordwrap($result , 3 , ' ' , true );
                    break;
                case 7:
                    $results[$key] = wordwrap($result , 2 , ' ' , true );
                    break;
            }
        }

        $data = Result::firstOrNew(['result' => json_encode($results)], ['result_province_id' => 22, 'shift_id'=> 2,'lottery_type_id' => 1]);
        $data->created_at = $date;
        $data->save();
        return $date;
    }
    function getNightResultFilter($day, $date){
        $province= ResultProvinceFilter::where([
            'location_type_id'=> 1,
            'weekly_id'=> $day
        ])->first();

        $result= Result::where('result_province_id','=',$province->result_province_id)->first();
        $results= json_decode($result->result);
        $post=5; /* 5 posts */

        for($i=1;$i<=$post;$i++){
            if($i!=1 or $i!=2){
                $data = new ResultFilter();
                $data->result_id= $result->id;
                $data->post_id = $i;
                $data->location_type_id= 1;
                $data->created_at= $date;
            }

            switch ($i){
                case 1:
                    for($x=1;$x<count($results)-2;$x++){
                        $result_data=explode(' ',$results[$x]);
                        foreach ($result_data as $item){
                            $data = new ResultFilter();
                            $data->result_id= $result->id;
                            $data->post_id = $i;
                            $data->location_type_id= 1;
                            $data->{'2D'} = substr($item, -2);
                            $data->{'3D'} = substr($item, -3);
                            $data->created_at= $date;
                            $data->save();
                        }
                    }
                    break;

                case 2:
                    $result_data= array();
                    for($x=6;$x<count($results);$x++) {
                        $result_data[$x-6] = explode(' ', $results[$x]);
                    }
                    for($y=0;$y< count($result_data[1]); $y++){
                        $data = new ResultFilter();
                        $data->result_id= $result->id;
                        $data->post_id = $i;
                        $data->location_type_id= 1;
                        $data->{'2D'} = $result_data[1][$y];
                        $data->{'3D'} = array_key_last($result_data[1]) != $y ? $result_data[0][$y]:null;
                        $data->created_at= $date;
                        $data->save();
                    }
                    break;

                case 3:
                    $data->{'2D'} = substr($results[0], 0,2);
                    $data->{'3D'} = substr($results[0], -3);
                    $data->save();
                    break;

                case 4:
                    $data->{'2D'} = substr($results[0], -2);
                    $data->{'3D'} = substr($results[0], 0, -2);
                    $data->save();
                    break;

                case 5:
                    $data->{'2D'} = substr($results[0], 0, -4).substr($results[0], -1);
                    $data->{'3D'} = substr($results[0], 1, -1);
                    $data->save();
                    break;
            }
        }
    }

    /******* Winner ******/
    function winnerIndex(){
        $locations= null;$users= null;
        if(!auth()->user()->hasRole('Dealer')) {
            if(auth()->user()->hasRole('Admin')) {
                $locations = Location::get();
            }
            $users= app('App\Http\Controllers\admin\BetController')->getUserByRole(auth()->user());
        }
        $shifts = Shift::get();
        $posts= auth()->user()->location->posts->where('lottery_type_id',1);
        return view('admin.winner',compact('shifts','locations','users','posts'));
    }
    function showWinner(Request $request){
        $location = empty($request->location) ? 'default': $request->location;
        $date_range=explode(' - ', $request->date_range);
        $min_date= date_format(date_create($date_range[0]), 'Y-m-d');
        $max_date= date_format(date_create($date_range[1]), 'Y-m-d');
        $post= explode(",",$request->post);

        $users= User::get();
        $user_id=array();
        $current_user = $request->user == 'default' || empty($request->user) ? auth()->user() : $users->where('id',$request->user)->first();
        foreach (app('App\Http\Controllers\admin\BetController')->getUserByRole($current_user) as $key => $user){
            $user_id[$key] = $user->id;
        }

        $winners = Winner::
        whereHas('post', function($item) use($post) {
            $item->whereIn('post',  $post);
        })
            ->whereIn('user_id',$user_id)
            ->where('shift_id',$request->shift)
            ->whereDate('created_at', '>=', $min_date)
            ->whereDate('created_at', '<=', $max_date)
            ->get();

        $winners = $winners
            ->when(($location != 'default') , function ($query) use($location){
                return $query->where('user.location_id', $location);
            });

        $new_winners=new Collection();
        foreach ($winners as $key => $winner){
            $new_winners->push((object)[
                'win_number'=> $winner->win_number,
                'bet_amount'=> $winner->bet_amount,
                'win_amount'=> $winner->win_amount,
                'post'=> $winner->post->post,
                'ticket'=> $winner->bet->ticket,
                'shift'=> $winner->shift->shift,
                'dealer'=> $winner->user->name,
                'payee'=> $winner->payee,
                'status'=> $winner->status,
                'digit' => strlen($winner->win_number),
            ]);
        }

        $columns = array(
            0 =>'no',
            1 =>'win_number',
            2=>'bet_amount',
            3=>'win_amount',
            4=>'post',
            5=>'ticket',
            6=>'shift',
            7=>'dealer',
            8=>'payee',
            9=>'status'
        );

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $sort= $dir == 'asc' ? 'sortBy':'sortByDesc';

        $new_winners = $new_winners
            ->where('digit', $request->digit)
            ->$sort($order);

        $totalData = $new_winners->count();
        $totalFiltered = $totalData;

        if(empty($request->input('search.value')))
        {
            $new_winners =$new_winners
                ->$sort($order)
                ->slice($start,$limit);
        }else {
            $search = $request->input('search.value');

            $new_winners =$new_winners
                ->filter(function ($item) use ($search) {
                    return false !== stristr($item, $search);
                })->$sort($order)
                ->slice($start,$limit);

            $totalFiltered = $new_winners
                ->filter(function ($item) use ($search) {
                    return false !== stristr($item, $search);
                })
                ->count();
        }

        $data = array();
        if(!empty($new_winners)){
            foreach ($new_winners as $key=>$winner){
                $nestedData['no'] = $key+1;
                $nestedData['win_number'] = $winner->win_number;
                $nestedData['bet_amount'] = $winner->bet_amount;
                $nestedData['win_amount'] = $winner->win_amount;
                $nestedData['post'] = $winner->post;
                $nestedData['ticket'] = $winner->ticket;
                $nestedData['shift'] = $winner->shift;
                $nestedData['dealer'] = $winner->dealer;
                $nestedData['payee'] = $winner->payee;
                $nestedData['status'] = $winner->status;
                $data[] = $nestedData;
            }

        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }
    function getWinner(){
        $date= Carbon::today()->addDay(-1)->toDateString();
        //$date= Carbon::today()->toDateString();
        if(!(Winner::whereDate('created_at', '=', $date)->exists())) {
            //$result=ResultFilter::whereDate('created_at', date('Y-m-d',strtotime(BetDetail::max('created_at'))))->orderBy('created_at','desc')->get();

            /** Get all today BetDetail **/
            $bets = Bet::whereDate('created_at', $date)->get();

            /** Get today Result **/
            $results = ResultFilter::whereDate('created_at', $date)->get();
            $win_count= 0;
            foreach ($bets as $bet) {
                foreach ($bet->bet_details as $bet_detail) {
                    /** Collect Post ID for each Betting **/
                    $post_id = new Collection();
                    foreach ($bet_detail->posts as $post) {
                        $post_id->push($post->id);
                    }

                    /** Count Bet Detail Digit (get the first in collection as sample) **/
                    $count = strlen(collect(json_decode($bet_detail->bet_number))->first());
                    $digits = $count . 'D';

                    /** Compare Bet Detail number and Result **/
                    $winners = $results
                        ->whereIn($digits, json_decode($bet_detail->bet_number))
                        ->whereIn('post_id', $post_id)
                        ->where('result.shift_id', $bet->shift->id)
                        ->where('result.lottery_type_id', $bet->lottery_type->id)
                        ->where('location_type_id', $bet->user->location->location_type->id)
                        ->whereIn('result.result_province_id',json_decode($bet_detail->province));

                    if ($winners->isNotEmpty()) {
                        $win_count= $winners->count();
                        foreach ($winners as $winner) {
                            $setting = $bet->user->user_settings->where('digit', $count)->first();
                            $win_amount = $bet_detail->amount * $setting->bet_win;
                            $data = Winner::firstOrNew(['win_number' => $winner->$digits, 'post_id' => $winner->post_id, 'bet_id' => $bet->id], ['bet_amount' => $bet_detail->amount, 'win_amount' => $win_amount, 'shift_id' => $winner->result->shift_id, 'user_id' => $bet->user->id]);
                            $data->save();
                        }
                    }
                }

            }
            $msg = 'Today has '.$win_count.' Number was Win';
        }
        else{
            $msg = 'No Win Number Updated';
        }
        return response()->json(array('msg'=> $msg), 200);

    }
}
