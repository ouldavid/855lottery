<?php

namespace App\Http\Controllers\admin;

use App\Models\BetDetail;
use App\Models\Location;
use App\Models\Post;
use App\Models\Shift;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;

class BetController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $locations= null;$users= null; $admin= false;
        if(!auth()->user()->hasRole('Dealer')) {
            if(auth()->user()->hasRole('Admin')) {
                $locations = Location::get();
                $admin= true;
            }
            $users= $this->getUserByRole(auth()->user());
        }
        $posts= $admin ? Post::where('lottery_type_id',1)->get() : auth()->user()->location->posts->where('lottery_type_id',1);
        return view('admin.bet',compact('locations','users','posts'));
    }

    function show(Request $request){
        $location = empty($request->location) ? 'default': $request->location;
        $date_range=explode(' - ', $request->date_range);
        $min_date= date_format(date_create($date_range[0]), 'Y-m-d');
        $max_date= date_format(date_create($date_range[1]), 'Y-m-d');
        /*dd($request->post);
        $post= $request->post != null? explode(",",$request->post):null;*/

        $users= User::get();
        $user_id=array();
        $current_user = $request->user == 'default' || empty($request->user) ? auth()->user() : $users->where('id',$request->user)->first();
        foreach ($this->getUserByRole($current_user) as $key => $user){
            $user_id[$key] = $user->id;
        }

        $bets = BetDetail::
            whereHas('posts', function($item) use($request) {
                if($request->post!=null) $item->whereIn('post',  $request->post);
            })
            ->whereHas('bet', function($item) use($user_id) {
                $item->whereIn('user_id',$user_id);
            })
            ->whereDate('created_at', '>=', $min_date)
            ->whereDate('created_at', '<=', $max_date)
            ->get();

        $bets = $bets
            ->where('bet.shift_id',$request->shift)
            ->when(($location != 'default') , function ($query) use($location){
                return $query->where('bet.user.location_id', $location);
            });

        $new_bets=new Collection();
        foreach ($bets as $key => $bet){
            $bet_numbers = json_decode($bet->bet_number);
            $posts=array();
            foreach ($bet->posts as $post){
                $posts[]=$post->id;
            }

            if(count($bet_numbers) > 1){
                foreach ($bet_numbers as $index=> $bet_number){
                    $new_bets->push((object)[
                        'bet_number'=> [$bet_number],
                        'sub_total'=> $bet->amount,
                        'digit' => strlen($bet_number),
                        'post'=> $posts
                    ]);
                }
            }
            else{
                $new_bets->push((object)[
                    'bet_number'=> json_decode($bet->bet_number),
                    'sub_total'=> $bet->amount,
                    'digit' => strlen(json_decode($bet->bet_number)[0]),
                    'post'=> $posts
                ]);
            }
        }

        $columns = array(
            0 =>'',
            1 =>'bet_number',
            2 =>'sub_total',
        );

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $sort= $dir == 'asc' ? 'sortBy':'sortByDesc';

        $new_bets= $new_bets
            ->where('digit', $request->digit)
            ->groupBy('bet_number')
            ->map(function ($row) {
                return collect(['bet_number'=>$row->first()->bet_number[0], 'sub_total'=>$row->sum('sub_total'),'digit'=>strlen($row->first()->bet_number[0])]);
            })
            ->$sort($order);

        $totalData = $new_bets->count();
        $totalFiltered = $totalData;

        if(empty($request->input('search.value'))) {
            $new_bets =$new_bets->slice($start,$limit);
        }
        else {
            $search = $request->input('search.value');
            $new_bets = $new_bets
                ->filter(function ($item) use ($search) {
                    return false !== stristr($item['bet_number'], $search);
                })
                ->slice($start,$limit);

            //Get total(count) bet after search
            $totalFiltered = $new_bets->count();
        }

        $data = array();
        if(!empty($new_bets)){
            foreach ($new_bets as $new_bet){

                $bet_detail =  $bets
                    ->filter(function ($item) use ($new_bet) {
                        return false !== stristr($item->bet_number, '"'.$new_bet['bet_number'].'"');
                    });
                $bet_collection=new Collection();
                foreach ($bet_detail as $key=> $bet){

                    // Get all Bet Number show as default (Ex: 10 -> 19)
                    $bet_numbers = json_decode($bet->bet_number);
                    $bet_num= implode(array_unique(str_split(implode($bet_numbers))));
                    switch ($bet->type){
                        case 'box':
                            $bet_num .= ' x '.strlen($bet_numbers[0]);
                            break;
                        case 'default':
                            $bet_num= $bet_numbers[0];
                            break;
                        default:
                            if($bet->type == 'roll-first') $operator = '↑';
                            else if($bet->type == 'roll-middle') $operator = '←';
                            else $operator = '↓';

                            $bet_num = $bet_numbers[0].' '.$operator.' '.$bet_numbers[count($bet_numbers)-1];
                            break;
                    }

                    $posts='';
                    foreach ($bet->posts as $post){
                        $posts .= $post->post;
                    }

                    $bet_collection->push((object)[
                        'dealer' => $bet->bet->user->name,
                        'post' => $posts,
                        'bet_num'=> $bet_num,
                        'amount'=> $bet->amount,
                        'ticket'=>$bet->bet->ticket,
                        'time' => $bet->created_at->isoFormat('h:mm:ss A'),
                        'date' => $bet->created_at->isoFormat('DD/MM/Y')
                    ]);
                }

                $nestedData['bet_number'] = $new_bet['bet_number'].' ('.$bet_collection->count().') ';
                $nestedData['sub_total'] = $new_bet['sub_total'];
                $nestedData['bet_data'] = $bet_collection;
                $data[] = $nestedData;
            }

        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        echo json_encode($json_data);
    }

    function getUserByRole($current_user, $self_id = false, $parent= false){
        switch ($current_user->roles->first()->name){
            case 'Admin':
                Admin:
                $users = User::whereHas('roles', function($query) {
                    $query->whereNotIn('role_id', [3]);
                })
                    ->when($parent,function ($query) use ($current_user) {
                        return $query->where('user_parent_id',$current_user->id);
                    })
                    ->where('is_activated', '1')
                    ->where('id', '!=', 1)
                    ->get();
                break;
            case 'Master':
                $users = User::where('user_parent_id', $current_user->id)
                    ->when($self_id,function ($query) use ($current_user) {
                        return $query->orWhere('id',$current_user->id);
                    })
                    ->where('is_activated', '1')
                    ->get();
                break;
            case 'Account':
                if($current_user->user_parent_id == 1){
                    goto Admin;
                }
                else{
                    $users = User::where('user_parent_id', $current_user->user_parent_id)
                        ->where('is_activated', '1')
                        ->get();
                }
                break;
            default:
                $users = User::where('id', $current_user->id)
                    ->where('is_activated', '1')
                    ->get();
                break;
        }
        return $users;
    }

    function ajaxUser(Request $request){
        $parent= isset($request->parent)? true: false;
        if($request->location != 'default'){
            $users= $this->getUserByRole(auth()->user(),false,$parent);
            $agents = $users->where('location_id',$request->location);
        }
        else{
            $agents= $this->getUserByRole(auth()->user(),false,$parent);
        }
        $data=array();
        foreach ($agents as $key => $agent){
            $data[$key]=[
                'agent_id' => $agent->id,
                'agent_name'=>$agent->name,
                'agent_role'=> $agent->roles->first()->name
            ];
        }
        return response()->json($data);
    }

}
