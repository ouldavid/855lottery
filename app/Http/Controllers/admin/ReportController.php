<?php

namespace App\Http\Controllers\admin;

use App\Models\Bet;
use App\Models\BetDetail;
use App\Models\Location;
use App\Models\Shift;
use App\Models\Transaction;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $locations= null;$users= null;
        if(!auth()->user()->hasRole('Dealer')) {
            if(auth()->user()->hasRole('Admin')) {
                $locations = Location::get();
            }
            $users= app('App\Http\Controllers\admin\BetController')->getUserByRole(auth()->user());
        }
        $actions= Transaction::select('action')->distinct()->get();

        return view('admin.transaction',compact('locations','users','actions'));

    }

    function show(Request $request){
        $date_range=explode(' - ', $request->date_range);
        $min_date= date_format(date_create($date_range[0]), 'Y-m-d');
        $max_date= date_format(date_create($date_range[1]), 'Y-m-d');

        $users= User::get();
        $user_id=array();
        $current_user = $request->user == 'default' || empty($request->user) ? auth()->user() : $users->where('id',$request->user)->first();
        foreach (app('App\Http\Controllers\admin\BetController')->getUserByRole($current_user,true) as $key => $user){
            $user_id[$key] = $user->id;
        }

        $transactions = Transaction::
            whereHas('account', function($item) use($user_id) {
                $item->whereIn('user_id',$user_id);
            })
            ->whereDate('created_at', '>=', $min_date)
            ->whereDate('created_at', '<=', $max_date)
            ->when(($request->action != 'default') , function ($query) use($request){
                return $query->where('action',$request->action);
            })
            ->get();

        if(!empty($request->location) && $request->location != 'default'){
            $transactions = $transactions->where('account.user.location_id',$request->location);
        }
        $new_transactions=new Collection();
        foreach ($transactions as $key => $transaction){
            $user_transaction='';
            $user= $transaction->account->user->name == auth()->user()->name ? 'You':$transaction->account->user->name;
            $user_parent= $users->where('id',$transaction->account->user->user_parent_id)->first()->name == auth()->user()->name ? 'You': $users->where('id',$transaction->account->user->user_parent_id)->first()->name;

            if($transaction->action == 'bet' || $transaction->action == 'lose'){
                $user_transaction .= $user.' <b>'.ucwords($transaction->action).'</b>';
            }
            else{
                $user_transaction .= $user_parent.' <b>'.ucwords($transaction->action).'</b>'.
                    ($transaction->action== 'deposit' ?' to ':' from ').$user;
            }

            $new_transactions->push((object)[
                'user_transaction'=> $user_transaction,
                'amount'=> $transaction->amount,
                /*'balance'=> $transaction->action == 'd' ? ($transaction->last_balance + $transaction->amount) : ($transaction->last_balance - $transaction->amount),*/
                'last_balance'=> $transaction->last_balance,
                /*'action' => $transaction->action,*/
                'noted'=> $transaction->noted,
                'date' => $transaction->created_at->isoFormat('DD-MM-Y h:mm:ss A')
            ]);
        }

        $columns = array(
            0 => "no" ,
            1 => "user_transaction",
            2 => "amount",
           /* 4 => "balance",*/
            3 => "last_balance",
            /*5 => "action",*/
            4 => "noted",
            5 => "date",
        );

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $sort= $dir == 'asc' ? 'sortBy':'sortByDesc';


        $totalData = $new_transactions->count();
        $totalFiltered = $totalData;

        if(empty($request->input('search.value'))) {
            $new_transactions =$new_transactions->$sort($order)->slice($start,$limit);
        }
        else {
            $search = $request->input('search.value');
            $new_transactions = $new_transactions
                ->filter(function ($item) use ($search) {
                    return false !== stristr($item, $search);
                })
                ->$sort($order)
                ->slice($start,$limit);

            //Get total(count) transaction after search
            $totalFiltered = $new_transactions
                ->filter(function ($item) use ($search) {
                    return false !== stristr($item, $search);
                })
                ->count();
        }

        $data = array();
        if(!empty($new_transactions)){
            foreach ($new_transactions as $key=>$transaction){
                $nestedData['no'] = $key+1;
                $nestedData['user_transaction'] = $transaction->user_transaction;
                $nestedData['amount'] = $transaction->amount;
                /*$nestedData['balance'] = $transaction->balance;*/
                $nestedData['last_balance'] = $transaction->last_balance;
                /*$nestedData['action'] = ucwords($transaction->action);*/
                $nestedData['noted'] = $transaction->noted;
                $nestedData['date'] = $transaction->date;
                $data[] = $nestedData;
            }

        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    public function profitIndex(){
        $locations= null;$users= null;
        if(!auth()->user()->hasRole('Dealer')) {
            if(auth()->user()->hasRole('Admin')) {
                $locations = Location::get();
            }
            $users= app('App\Http\Controllers\admin\BetController')->getUserByRole(auth()->user(),false,true);
        }
        $shifts = Shift::get();
        return view('admin.profit',compact('shifts','locations','users'));

    }

    function profitShow(Request $request){
        $location = empty($request->location) ? 'default': $request->location;
        $date_range=explode(' - ', $request->date_range);
        $min_date= date_format(date_create($date_range[0]), 'Y-m-d');
        $max_date= date_format(date_create($date_range[1]), 'Y-m-d');

        $users= User::get();
        $user_id=array();
        $current_user = $request->user == 'default' || empty($request->user) ? auth()->user() : $users->where('id',$request->user)->first();
        foreach (app('App\Http\Controllers\admin\BetController')->getUserByRole($current_user) as $key => $user){
            $user_id[$key] = $user->id;
        }

        $bets = Bet::selectRaw('user_id, sum(grand_total) as total_bet')
            ->groupBy('user_id')
            ->whereIn('user_id',$user_id)
            ->where('shift_id',$request->shift)
            ->whereDate('created_at', '>=', $min_date)
            ->whereDate('created_at', '<=', $max_date)
            ->get();

        $bets = $bets
            ->when(($location != 'default') , function ($query) use($location){
                return $query->where('user.location_id', $location);
            });

        $profits=new Collection();
        foreach ($bets as $key => $profit){
            $total_lose = $profit->user->winners
                ->whereBetween('created_at', [$min_date.' 00:00:00', $max_date.' 23:59:59'])
                ->sum('win_amount');

            $profits->push((object)[
                'agent'=> $profit->user->name,
                'total_bet' => $profit->total_bet,
                'total_lose' => $total_lose,
                'sub_total'=> ((int)$profit->total_bet - (int)$total_lose),
                'parent' => $profit->user->user_parent_id
            ]);
        }


        if($request->user == 'default'){
            $profits= $profits
                ->groupBy('parent')
                ->map(function ($row) use ($users) {
                    return (object)([
                        'agent'=> $users->where('id',$row->first()->parent)->first()->name,
                        'total_bet'=> $row->sum('total_bet'),
                        'total_lose'=> $row->sum('total_lose'),
                        'sub_total'=> ((int)$row->sum('total_bet') - (int)$row->sum('total_lose')),
                    ]);
                })->values();
        }


        $columns = array(
            0 =>'no',
            1 =>'agent',
            2 =>'total_bet',
            3 =>'total_lose',
            4 =>'sub_total',
        );

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $sort= $dir == 'asc' ? 'sortBy':'sortByDesc';

        $totalData = $profits->count();
        $totalFiltered = $totalData;

        if(empty($request->input('search.value'))) {
            $profits =$profits->$sort($order)->slice($start,$limit);
        }
        else {
            $search = $request->input('search.value');
            $profits = $profits->$sort($order)
                ->filter(function ($item) use ($search) {
                    return false !== stristr($item, $search);
                })
                ->slice($start,$limit);

            //Get total(count) bet after search
            $totalFiltered = $profits
                ->count();
        }

        $data = array();
        if(!empty($profits)){
            foreach ($profits as $key=>$profit){
                $nestedData['no'] = $key+1;
                $nestedData['agent'] = $profit->agent;
                $nestedData['total_bet'] = number_format($profit->total_bet);
                $nestedData['total_lose'] = number_format($profit->total_lose);
                $nestedData['sub_total'] = number_format($profit->sub_total);
                $data[] = $nestedData;
            }

        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        echo json_encode($json_data);
    }

    public function betReportIndex(){
        $locations= null;$users= null;
        if(!auth()->user()->hasRole('Dealer')) {
            if(auth()->user()->hasRole('Admin')) {
                $locations = Location::get();
            }
            $users= app('App\Http\Controllers\admin\BetController')->getUserByRole(auth()->user(),false,true);
        }
        $shifts = Shift::get();
        return view('admin.bet-report',compact('shifts','locations','users'));

    }

    function betReportShow(Request $request){
        $location = empty($request->location) ? 'default': $request->location;
        $date_range=explode(' - ', $request->date_range);
        $min_date= date_format(date_create($date_range[0]), 'Y-m-d');
        $max_date= date_format(date_create($date_range[1]), 'Y-m-d');

        $users= User::get();
        $user_id=array();
        $current_user = $request->user == 'default' || empty($request->user) ? auth()->user() : $users->where('id',$request->user)->first();
        foreach (app('App\Http\Controllers\admin\BetController')->getUserByRole($current_user) as $key => $user){
            $user_id[$key] = $user->id;
        }

        $bets = BetDetail::whereHas('bet', function($item) use($user_id) {
            $item->whereIn('user_id',$user_id);
        })
            ->whereDate('created_at', '>=', $min_date)
            ->whereDate('created_at', '<=', $max_date)
            ->get();

        $bets = $bets
            ->when(($location != 'default') , function ($query) use($location){
                return $query->where('user.location_id', $location);
            })
            ->where('bet.shift_id',$request->shift);

        $new_bets=new Collection();
        foreach ($bets as $key => $bet){
            $bet_numbers = json_decode($bet->bet_number);
            $discount = $bet->amount * (100 - $bet->discount)/100;
            if(count($bet_numbers) > 1){
                foreach ($bet_numbers as $index=> $bet_number){
                    $new_bets->push((object)[
                        'bet_number'=> [$bet_number],
                        'user_id'=> $bet->bet->user_id,
                        'sub_total'=> ($bet->amount - $discount) * ($bet->bet_multiply - (count($bet_numbers)-1)),
                    ]);
                }
            }
            else{
                $new_bets->push((object)[
                    'bet_number'=> json_decode($bet->bet_number),
                    'user_id'=> $bet->bet->user_id,
                    'sub_total'=> ($bet->amount - $discount) * $bet->bet_multiply,
                ]);
            }
        }

        $new_bets= $new_bets
            ->groupBy('user_id')
            ->map(function ($row) {
                return collect(['user'=>$row->first()->user_id, 'sub_total'=>$row->sum('sub_total'),'digit'=>strlen($row->first()->bet_number[0])]);
            })->sortByDesc('sub_total');

        $data = array();
        if(!empty($new_bets)){
            for($i=0;$i<3;$i++){
                foreach ($new_bets->where('digit',$i+2)->values() as $key=>$item){
                    $data[$key]['no']= $key+1;
                    $data[$key]['user']= $item['user'];
                    $data[$key][($i+2).'digit'] = number_format($item['sub_total']);
                }
            }
        }

        /*dd($data);
        $profits=new Collection();
        foreach ($bets as $key => $profit){
            $total_lose = $profit->user->winners
                ->whereBetween('created_at', [$min_date.' 00:00:00', $max_date.' 23:59:59'])
                ->sum('win_amount');

            $profits->push((object)[
                'agent'=> $profit->user->name,
                'total_bet' => $profit->total_bet,
                'total_lose' => $total_lose,
                'sub_total'=> ((int)$profit->total_bet - (int)$total_lose),
                'parent' => $profit->user->user_parent_id
            ]);
        }


        if($request->user == 'default'){
            $profits= $profits
                ->groupBy('parent')
                ->map(function ($row) use ($users) {
                    return (object)([
                        'agent'=> $users->where('id',$row->first()->parent)->first()->name,
                        'total_bet'=> $row->sum('total_bet'),
                        'total_lose'=> $row->sum('total_lose'),
                        'sub_total'=> ((int)$row->sum('total_bet') - (int)$row->sum('total_lose')),
                    ]);
                })->values();
        }*/


        $columns = array(
            0 =>'no',
            1 =>'user',
            2 =>'2digit',
            3 =>'3digit',
            4 =>'4digit',
            /*4 =>'sub_total',*/
        );

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        //$sort= $dir == 'asc' ? 'sort':'rsort';

        $totalData = count($data);
        $totalFiltered = $totalData;

        if(empty($request->input('search.value'))) {
            //$this->sortBy($order,$data, $dir);
            $data = array_slice($data,$start,$limit);
            //$data =$data->$sort($order)->slice($start,$limit);
        }
        else {
            /*$search = $request->input('search.value');
            $data = $data->$sort($order)
                ->filter(function ($item) use ($search) {
                    return false !== stristr($item, $search);
                })
                ->slice($start,$limit);

            //Get total(count) bet after search
            $totalFiltered = count($data);*/
        }

        /*$data = array();
        if(!empty($profits)){
            foreach ($profits as $key=>$profit){
                $nestedData['no'] = $key+1;
                $nestedData['agent'] = $profit->agent;
                $nestedData['total_bet'] = number_format($profit->total_bet);
                $nestedData['total_lose'] = number_format($profit->total_lose);
                $nestedData['sub_total'] = number_format($profit->sub_total);
                $data[] = $nestedData;
            }

        }*/
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        echo json_encode($json_data);
    }

    function sortBy($field, &$array, $direction = 'asc'){
        usort($array, function($a, $b) use ($direction, $field) {
            $a = $a[$field];
            $b = $b[$field];

            if ($a == $b) return 0;

            $direction = strtolower(trim($direction));

            return ($a . ($direction == 'desc' ? '>' : '<') . $b) ? -1 : 1;
        });

        return true;
    }
}
