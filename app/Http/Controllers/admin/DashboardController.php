<?php

namespace App\Http\Controllers\admin;

use App\Models\Bet;
use App\Models\BetDetail;
use App\Models\Setting;
use App\Models\Winner;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        /*$bets = BetDetail::get()
            ->groupBy(function($row){
                $bet= json_decode($row->bet_number);
                return strlen($bet[0]);
            })
            ->map(function ($rows) {
                $bet= json_decode($rows[0]->bet_number);

                return collect([
                    'label'=> strlen($bet[0]),
                    'fillColor'=> 'rgba(210, 214, 222, 1)',
                    'strokeColor'=> 'rgba(210, 214, 222, 1)',
                    'pointColor'=> 'rgba(210, 214, 222, 1)',
                    'pointStrokeColor'=> '#c1c7d1',
                    'pointHighlightFill'=> '#fff',
                    'pointHighlightStroke'=> 'rgba(220,220,220,1)',
                    'data'=> [$rows->sum('amount')]
                ]);
            });
        dd($bets);*/

        // user access
        if(!auth()->user()->hasAnyRole(['Admin','Master','Account'])) {
            abort(401, 'This action is unauthorized.');
        }

        $users = app('App\Http\Controllers\admin\BetController')
            ->getUserByRole(auth()->user());
        // return page
        return view('admin.dashboard',compact('users'));
    }

    function showBetting(Request $request){
        $user_id=array();
        foreach (app('App\Http\Controllers\admin\BetController')->getUserByRole(auth()->user()) as $key => $user){
            $user_id[$key] = $user->id;
        }

        $bets = BetDetail::whereHas('bet', function($item) use($user_id) {
                $item->whereIn('user_id',$user_id);
            })
            ->whereDate('created_at', '=', Carbon::today()->toDateString())
            ->get();

        $new_bets=new Collection();
        foreach ($bets as $key => $bet){
            $bet_numbers = json_decode($bet->bet_number);
            if(count($bet_numbers) > 1){
                foreach ($bet_numbers as $index=> $bet_number){
                    $new_bets->push((object)[
                        'bet_number'=> [$bet_number],
                        'sub_total'=> $bet->amount,
                    ]);
                }
            }
            else{
                $new_bets->push((object)[
                    'bet_number'=> json_decode($bet->bet_number),
                    'sub_total'=> $bet->amount,
                ]);
            }
        }

        $new_bets= $new_bets
            ->groupBy('bet_number')
            ->map(function ($row) {
                return collect(['bet_number'=>$row->first()->bet_number[0], 'sub_total'=>$row->sum('sub_total'),'digit'=>strlen($row->first()->bet_number[0])]);
            })->sortByDesc('sub_total');

        $data = array();
        if(!empty($new_bets)){
            for($i=0;$i<3;$i++){
                foreach ($new_bets->where('digit',$i+2)->values()->take(10) as $key=>$item){
                    $data[$key]['no']= $key+1;
                    $data[$key][($i+2).'digit'] = $item['bet_number'].' ('.number_format($item['sub_total']).')';
                }
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "data"            => $data
        );
        echo json_encode($json_data);
    }

    function showLose(Request $request){
        $user_id=array();
        foreach (app('App\Http\Controllers\admin\BetController')->getUserByRole(auth()->user()) as $key => $user){
            $user_id[$key] = $user->id;
        }

        $loses = Winner::selectRaw('win_number, sum(win_amount) as total')
            ->whereIn('user_id',$user_id)
            ->whereDate('created_at', '=', Carbon::today()->toDateString())
            ->groupBy('win_number')
            ->orderBy('total','Desc')
            ->get();

        $data = array();
        if(!empty($loses)){
            foreach ($loses->take(10) as $key=>$lose){
                $nestedData['no'] = $key+1;
                $nestedData['number'] = $lose->win_number;
                $nestedData['total'] = $lose->total;
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "data"            => $data
        );
        echo json_encode($json_data);
    }

    function showData(Request $request){
        $user_id=array();
        foreach (app('App\Http\Controllers\admin\BetController')->getUserByRole(auth()->user()) as $key => $user){
            $user_id[$key] = $user->id;
        }
        $color = json_decode(Setting::where('key','color')->first()->value);

        $location_bets = Bet::whereDate('created_at', '=', Carbon::today()->toDateString())
            ->get()
            ->groupBy('user.location.id')
            ->map(function ($row) use ($color) {
                return collect(['value'=>$row->sum('grand_total'),'color'=> $color[$row->first()->user->location->id],'highlight'=>$color[$row->first()->user->location->id], 'label'=>$row->first()->user->location->location]);
            })->sortByDesc('value');

        $data=[
          'location_bet'=> $location_bets,
          'post_bet'  => ''
        ];

        return response()->json($data);
    }
}
