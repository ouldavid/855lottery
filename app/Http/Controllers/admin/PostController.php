<?php

namespace App\Http\Controllers\admin;

use App\Models\PostSetting;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $posts= PostSetting::get();
        //dd($posts);

        return view('admin.post',compact('posts'));
    }
}
