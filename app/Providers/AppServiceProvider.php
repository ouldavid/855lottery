<?php

namespace App\Providers;

use App\Models\Shift;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Jenssegers\Agent\Agent as Agent;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Migrate table
        Schema::defaultStringLength(191);

        // Title page
        view()->composer('*', function($view) {
            $view_name = substr(strrchr($view->getName(), '.'), 1);
            $data=[
                'view_name'=>ucfirst(trim($view_name)),
                'agent'=>new Agent(),
                'shifts'=>$shifts= Shift::all()
            ];

            view()->share($data);
        });

    }
}
