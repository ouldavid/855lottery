<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Bet extends Model
{
    protected $fillable = ['user_id','shift_id','lottery_type_id','total','grand_total','currency_id','ticket','user_parent_id'];

    public function bet_details(){
        return $this->hasMany(BetDetail::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function shift(){
        return $this->belongsTo(Shift::class);
    }

    public function lottery_type(){
        return $this->belongsTo(LotteryType::class);
    }

    public function winners(){
        return $this->hasMany(Winner::class);
    }

    public function transactions(){
        return $this->belongsToMany(Transaction::class);
    }

    public function currency(){
        return $this->belongsTo(Currency::class);
    }
}
