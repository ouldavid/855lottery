<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostSetting extends Model
{
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function shift()
    {
        return $this->belongsTo(Shift::class);
    }

    public function location_type()
    {
        return $this->belongsTo(LocationType::class);
    }

    public function lottery_type()
    {
        return $this->belongsTo(LotteryType::class);
    }
}
