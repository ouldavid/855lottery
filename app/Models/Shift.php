<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shift extends Model
{
    public function post_settings()
    {
        return $this->hasMany(PostSetting::class);
    }

    public function bets()
    {
        return $this->hasMany(Bet::class);
    }

    public function result_filters()
    {
        return $this->hasMany(ResultFilter::class);
    }
}
