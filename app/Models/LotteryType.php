<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LotteryType extends Model
{
    public function post_settings(){
        return $this->hasMany(PostSetting::class);
    }
    public function result_filters(){
        return $this->hasMany(ResultFilter::class);
    }
    public function bets(){
        return $this->hasMany(Bet::class);
    }
    public function locations(){
        return $this->belongsToMany(Location::class);
    }
    public function posts(){
        return $this->hasMany(Post::class);
    }
    public function user_settings(){
        return $this->hasMany(UserSetting::class);
    }

}
