<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function post_settings(){
        return $this->hasMany(PostSetting::class);
    }

    public function bet_details(){
        return $this->belongsToMany(BetDetail::class);
    }

    public function locations(){
        return $this->belongsToMany(Location::class);
    }

    public function result_filters(){
        return $this->hasMany(ResultFilter::class);
    }

    public function lottery_type(){
        return $this->belongsTo(LotteryType::class);
    }
}
