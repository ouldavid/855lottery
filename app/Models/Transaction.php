<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $guarded = array('id');

    public function account(){
        return $this->belongsTo(Account::class);
    }

    public function bets(){
        return $this->belongsToMany(Bet::class);
    }
}
