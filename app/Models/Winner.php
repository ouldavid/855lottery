<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Winner extends Model
{
    protected $fillable = ['win_number', 'bet_amount', 'win_amount', 'post_id', 'shift_id', 'bet_id', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function bet()
    {
        return $this->belongsTo(Bet::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function shift()
    {
        return $this->belongsTo(Shift::class);
    }
}
