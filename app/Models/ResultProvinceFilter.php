<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResultProvinceFilter extends Model
{
    public function location_type()
    {
        return $this->belongsTo(LocationType::class);
    }

    public function weekly()
    {
        return $this->belongsTo(Weekly::class);
    }

    public function result_province()
    {
        return $this->belongsTo(ResultProvince::class);
    }
}
