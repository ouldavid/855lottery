<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// define global variable
view()->share('version', time());

Route::get('/', 'HomeController@index')->name('home');
Route::get('/ajax-bet','HomeController@ajaxBet')->name('ajax-bet');
Route::post('/ajax-bet','HomeController@store')->name('store-bet');
Route::get('/ajax-onChange','HomeController@ajaxOnChange')->name('ajax-onChange');
Route::get('/ajax-ticket','HomeController@ajaxTicket')->name('ajax-ticket');
Route::get('/ajax-post','HomeController@ajaxPost')->name('ajax-post');
Route::get('/ajax-receipt','HomeController@ajaxReceipt')->name('ajax-receipt');

// Admin group
Route::group(['prefix' => 'admin'], function() {
    Auth::routes();
});
Route::get('/admin', function () {
    return redirect('/admin/dashboard');
});

Route::get('/admin/dashboard', 'admin\DashboardController@index')->name('dashboard');
Route::get('/admin/dashboard/show-betting', 'admin\DashboardController@showBetting')->name('dashboard.show-betting');
Route::get('/admin/dashboard/show-lose', 'admin\DashboardController@showLose')->name('dashboard.show-lose');
Route::get('/admin/dashboard/show-data', 'admin\DashboardController@showData')->name('dashboard.show-data');

Route::get('/admin/post', 'admin\PostController@index')->name('post');

// Bet
Route::get('/admin/bet','admin\BetController@index')->name('bet');
Route::post('/admin/bet/show','admin\BetController@show')->name('bet.show');
Route::get('/admin/bet/show', function() {
    return abort(404);
});
Route::get('/admin/bet/ajax-user','admin\BetController@ajaxUser')->name('ajax-user');

// users, roles
Route::get('/admin/user', 'admin\UserController@index')->name('user');
Route::post('/admin/user', 'admin\UserController@store');
Route::get('/admin/user/show', function() {
    return abort(404);
});
Route::post('/admin/user/show', 'admin\UserController@show')->name('user.show');
Route::get('/admin/user/edit', 'admin\UserController@show')->name('user.edit');
Route::post('/admin/user/update/{id}', 'admin\UserController@update')->name('user.update');
Route::post('/admin/user/update-setting', 'admin\UserController@updateSetting')->name('user.update-setting');
Route::get('/admin/user/ajax-setting/{id}', 'admin\UserController@ajaxSetting')->name('user.ajax-setting');
Route::get('/admin/user/ajax-parent', 'admin\UserController@ajaxParent')->name('user.ajax-parent');
Route::delete('/admin/user/destroy/{id}', 'admin\UserController@destroy')->name('user.destroy');
Route::post('/admin/user/reset-password/{id}', 'admin\UserController@resetPassword')->name('user.reset-password');
// roles
Route::get('/admin/role', 'admin\RoleController@index')->name('role');
Route::post('/admin/role', 'admin\RoleController@store');
Route::get('/admin/role/show', function() {
    return abort(404);
});
Route::post('/admin/role/show', 'admin\RoleController@show')->name('role.show');
Route::delete('/admin/role/destroy/{id}', 'admin\RoleController@destroy')->name('role.destroy');
Route::post('/admin/role/update/{id}', 'admin\RoleController@update')->name('role.update');

// profile
Route::get('/admin/profile', 'admin\ProfileController@index')->name('profile');
Route::post('/admin/profile', 'admin\ProfileController@update');
Route::post('/admin/profile/change-password', 'admin\ProfileController@changePassword')->name('profile.change-password');
Route::get('/admin/profile/change-password', function() {
    return abort(404);
});

// user balance
Route::post('/admin/user/deposit/{id}', 'admin\UserController@deposit')->name('user.deposit');
Route::post('/admin/user/withdraw/{id}', 'admin\UserController@withdraw')->name('user.withdraw');

// Result
Route::get('/admin/result','admin\ResultController@index')->name('result');
Route::get('/admin/result/show','admin\ResultController@show')->name('result.show');
Route::post('/admin/result/get','admin\ResultController@getResult')->name('result.get');
Route::get('/admin/result/get', function() {
    return abort(404);
});

/*Route::get('/admin/winner','admin\ResultController@getWinner')->name('winner');*/
Route::get('/admin/winner','admin\ResultController@winnerIndex')->name('winner');
Route::get('/admin/winner/show','admin\ResultController@showWinner')->name('winner.show');
Route::get('/admin/winner/ajax-user','admin\BetController@ajaxUser')->name('ajax-user');

Route::post('/admin/winner/get','admin\ResultController@getWinner')->name('winner.get');
Route::get('/admin/winner/get', function() {
    return abort(404);
});

// Transaction
Route::get('/admin/transaction','admin\ReportController@index')->name('transaction');
Route::get('/admin/transaction/show','admin\ReportController@show')->name('transaction.show');

//Profit & Lost
Route::get('/admin/profit','admin\ReportController@profitIndex')->name('profit');
Route::get('/admin/profit/show','admin\ReportController@profitShow')->name('profit.show');

//Bet Report
Route::get('/admin/bet-report','admin\ReportController@betReportIndex')->name('bet-report');
Route::get('/admin/bet-report/show','admin\ReportController@betReportShow')->name('bet-report.show');

//Setting
Route::get('/admin/setting','admin\SettingController@index')->name('setting');
Route::get('/admin/setting/currency','admin\SettingController@currencySetting')->name('setting.currency');
