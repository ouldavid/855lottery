<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'day' => 'ថ្ងៃ',
    'night' => 'យប់',
    'post' => 'ប៉ុស្ថិ៍',
    'number' => 'លេខ',
    'amount' => 'ចំនួន',
    'sub_total' => 'សរុប',
    'province' => 'ខេត្ត',
    'action' => 'កែប្រែ',
    'balance' => 'សម្យតុលទឹកប្រាក់',
    'total' => 'សរុប',
    'discount' => 'កាត់ទឹក',
    'grand_total' => 'សរុបទាំងអស់',
    'option' => 'គុណ/អូស',
    'new_ticket' => 'វគ្គថ្មី',
    'delete' => 'លុប',
    'bet' => 'ចាក់',
    'all'=> 'គ្រប់',
    'messages' => [
        'message_title'=>'សាររបស់អ្នក',
        'bet_success'=> "ការចាក់ឆ្នោតរបស់លោកអ្នកទទួលបានជោគជ័យ! សូមរក្សាកន្ទុយសំបុត្រដើម្បីផ្ទៀងផ្ទាត់លទ្ធផល។ សូមអរគុណ!",
        'updated_success'=> "ការកែប្រែលើសន្លឹកឆ្នោតរបស់លោកអ្នកទទួលបានជោគជ័យ! សូមរក្សាកន្ទុយសំបុត្រដើម្បីផ្ទៀងផ្ទាត់លេខឆ្នោត។ សូមអរគុណ!",
        'day_time_expired'=> "ផុតកំណត់ម៉ោងចាក់ពេលថ្ងៃ!",
        'night_time_expired'=> "ផុតកំណត់ម៉ោងចាក់ពេលយប់!",
        'not_enough_balance'=> "ទឹកប្រាក់របស់លោកអ្នកមិនគ្រប់គ្រាន់សម្រាប់ការចាក់នេះទេ!",
        'lottery_type'=>'តើអ្នកពិតជាចង់ប្ដូរប្រភេទឆ្នោតមែនទេ?',
        'yes'=>'បាទ',
        'no'=>'ទេ',
        'close'=>'បិទ'
    ],

];
