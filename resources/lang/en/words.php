<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'day' => 'Day',
    'night' => 'Night',
    'post' => 'Post',
    'number' => 'Number',
    'amount' => 'Amount',
    'sub_total' => 'Sub Total',
    'province' => 'Province',
    'action' => 'Action',
    'balance' => 'Balance',
    'total' => 'Total',
    'discount' => 'Discount',
    'grand_total' => 'Grand Total',
    'option' => 'Option',
    'new_ticket' => 'New',
    'delete' => 'Del',
    'bet' => 'Bet',
    'all'=> 'All',
    'messages' => [
        'message_title'=>'Message',
        'bet_success'=> "Your bet was success! Please keep your ticket, thanks you!",
        'updated_success'=> "Your bet was updated successfully! Please keep your new ticket, thanks you!",
        'day_time_expired'=> "The Day lottery bet timeout!",
        'night_time_expired'=> "Today lottery bet timeout!",
        'not_enough_balance'=> "Not enough balance to bet!",
        'lottery_type'=>'Are you sure want to change the lottery type?',
        'yes'=>'Yes',
        'no'=>'No',
        'close'=>'Close'
    ],

];
