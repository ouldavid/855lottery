<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') :: 855 Lottery</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="/favicon.ico?v={{ $version }}" type="image/x-icon" />

    {{--<!-- Styles -->
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">--}}

    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap/dist/css/bootstrap.min.css?v='.$version) }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('assets/bower_components/font-awesome/css/all.min.css?v='.$version) }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('assets/bower_components/Ionicons/css/ionicons.min.css?v='.$version) }}">
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-daterangepicker/daterangepicker.css?v='.$version) }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css?v='.$version) }}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{ asset('assets/bower_components/jvectormap/jquery-jvectormap.css?v='.$version) }}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css?v='.$version) }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/responsive.bootstrap.min.css?v='.$version) }}">
        <!-- Select2 -->
        <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css?v='.$version) }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('assets/dist/css/AdminLTE.min.css?v='.$version) }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('assets/dist/css/skins/_all-skins.min.css?v='.$version) }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <!-- Import CSS -->
    <link rel="stylesheet" href="{{ asset('css/fonts.css?v='.$version) }}">
    <link rel="stylesheet" href="{{ asset('assets/dist/css/style.css?v='.$version) }}">
    @yield('css')

</head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        <!-- sidebar -->
        @include('templates.admin.left-panel')
        <!-- header -->
        @include('templates.admin.header')
        <!-- content -->
        <div class="content-wrapper">
        @yield('content')
        </div>
        <!-- footer -->
        @include('templates.admin.footer')
    </div>

    <!-- msg success -->
    @if(session()->has('flash_success'))
    <div id="msg-success" class="col-xs-2 msg flash-message">
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <span class="glyphicon glyphicon-ok"></span> <strong>Saved</strong>
            {{--<hr class="message-inner-separator">
            <p>Your data was <strong class="text-success">Inserted</strong> successfully.</p>--}}
        </div>
    </div>
    @endif
    <!-- msg danger -->
    @if(session()->has('flash_danger'))
    <div id="msg-danger" class="col-xs-2 msg flash-message">
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <span class="glyphicon glyphicon-trash"></span> <strong>Deleted</strong>
            {{--<hr class="message-inner-separator">
            <p>Your data was <strong>Deleted</strong> successfully.</p>--}}
        </div>
    </div>
    @endif
    <!-- msg warning -->
    @if(session()->has('flash_warning'))
    <div id="msg-warning" class="col-xs-2 msg flash-message">
        <div class="alert alert-warning">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <span class="glyphicon glyphicon-warning-sign"></span> <strong>Nothing Modified</strong>
            {{--<hr class="message-inner-separator">
            <p>Your data wasn't <strong>Modified</strong>.</p>--}}
        </div>
    </div>
    @endif

{{--<!-- Scripts -->
<script src="{{ mix('/js/app.js') }}"></script>--}}
<!-- jQuery 3 -->
<script src="{{ asset('assets/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- date-range-picker -->
<script src="{{ asset('assets/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('assets/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/bower_components/datatables.net-bs/js/responsive.bootstrap.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('assets/bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/dist/js/adminlte.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap  -->
<script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('assets/bower_components/chart.js/Chart.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('assets/dist/js/pages/dashboard2.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('assets/dist/js/demo.js') }}"></script>
<!-- Import js -->
<script src="{{ asset('assets/dist/js/common.js?v='.$version) }}"></script>
<!-- Alert message -->
<script type="text/javascript">
    setInterval(function() {
        $('.flash-message').addClass('hide');
    }, 5000);
</script>
@yield('script')

</body>
</html>
