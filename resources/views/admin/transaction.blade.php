@extends('layouts.admin-master')

@section('title', $view_name)

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Transaction
            {{--<small>advanced tables</small>--}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fas fa-tachometer-alt"></i> Home</a></li>
            <li><a href="{{ route('transaction') }}">Transaction</a></li>
            <li class="active">List</li>
        </ol>
    </section>

    <div class="box-header">
        <div class="hidden-print with-border">
           {{-- <a href="#" class="btn btn-primary ladda-button" data-toggle="modal" data-target="#myModal" data-style="zoom-in">
            <span class="ladda-label"><i class="fa fa-plus"></i> Add Role</span></a>--}}
            <button class="btn btn-default border-radius" onclick="reload_page()"><i class="fas fa-redo"></i> Reload</button>
        </div>
    </div>
    <!-- /.box-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">

                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <button type="button" class="btn btn-default btn-block" id="date-range">
                                        <i class="fa fa-calendar"></i>&nbsp;
                                        <span></span>
                                        &nbsp;<i class="fa fa-caret-down"></i>
                                    </button>
                                </div>
                            </div>
                            @if(isset($locations))
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <select class="form-control" id="location">
                                            <option value="default" selected="selected">-- Location --</option>
                                            @foreach($locations as $location)
                                                <option value="{{$location->id}}">{{$location->location}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                            @if(isset($users))
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <select class="form-control" id="user">
                                            <option value="default" selected="selected">-- User --</option>
                                            @foreach($users as $user)
                                                <option value="{{$user->id}}">{{$user->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                            @if(isset($actions))
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <select class="form-control" id="action">
                                            <option value="default" selected="selected">-- Action --</option>
                                            @foreach($actions as $action)
                                                <option value="{{$action->action}}">{{ucwords($action->action)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <table id="table" class="table table-bordered table-hover dt-responsive">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Transaction</th>
                                <th>Amount</th>
                                {{--<th>Updated Balance</th>--}}
                                <th>Last Balance</th>
                                {{--<th>Action</th>--}}
                                <th>Noted</th>
                                <th>Date & Time</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>

@stop

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#date-range span').html(moment().format('MMM D, YYYY') + ' - ' + moment().format('MMM D, YYYY'));
            $('#date-range').daterangepicker(
                {
                    autoApply: true,
                    ranges   : {
                        'Today'       : [moment(), moment()],
                        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment(),
                    endDate  : moment(),
                    maxDate: moment(),
                    //alwaysShowCalendars: true,
                    showCustomRangeLabel: true,
                    opens: "right"
                },
                function (start, end) {
                    $('#date-range span').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'))
                }
            );

            let table = fetch_data();

            $('#date-range, #location, #user, #action').on('change apply.daterangepicker', function(){
                if($(this).attr('id')=='location'){
                    $.ajax({
                        type: 'GET',
                        url: '{{ route('ajax-user') }}',
                        data: {location: $('#location').val()},
                        success: function (data) {
                            const option = new Option('All', 'default', true, true);
                            $('#user').html(option);
                            $.each(data, function (index) {
                                const newOption = new Option(data[index].agent_name+' ( '+data[index].agent_role+' )', data[index].agent_id, false, false);
                                $('#user').append(newOption);
                            });
                        }
                    });
                }

                $('#table').DataTable().destroy();
                table = fetch_data();
            });



            function fetch_data() {
                // Data table for serverside
                const table = $('#table').DataTable({
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    ajax:{
                        url: "{{ route('transaction.show') }}",
                        dataType: "json",
                        type: "GET",
                        data:{
                            _token: "{{ csrf_token() }}",
                            route:'transaction',
                            date_range: $('#date-range span').html(),
                            user: $('#user').val(),
                            location: $('#location').val(),
                            action: $('#action').val()
                        }
                    },
                    columns: [
                        { data: "no" },
                        { data: "user_transaction" },
                        { data: "amount" },
                        /*{ data: "balance" },*/
                        { data: "last_balance" },
                        /*{ data: "action" },*/
                        { data: "noted" },
                        { data: "date" },
                    ],
                    pageLength: 100
                } );

                return table;
            }

        });
    </script>
@endsection
