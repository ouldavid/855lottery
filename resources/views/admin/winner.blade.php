@extends('layouts.admin-master')

@section('title', $view_name)

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Winner
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Winner</li>
        </ol>
    </section>

    <div class="box-header">
        <div class="hidden-print with-border">
            @if(auth()->user()->hasRole('Admin'))
                <a href="#" id="getWinner" class="btn btn-primary" data-toggle="modal" data-target="#winnerModal" data-style="zoom-in">
                    <span><i class="fas fa-sync"></i> Get Winner</span>
                </a>
            @endif
            <button class="btn btn-default border-radius" onclick="reload_page()"><i class="fas fa-redo"></i> Reload</button>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="winnerModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Message</h4>
                </div>
                <div class="modal-body">
                    <div id='loading'>
                        <img class="img-fluid" src="{{ asset("images/loading.gif?v=".$version) }}"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="reload_page()" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">

                        <!-- Filter Shift and Post Category -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <button type="button" class="btn btn-default btn-block" id="date-range">
                                        <i class="fa fa-calendar"></i>&nbsp;
                                        <span></span>
                                        &nbsp;<i class="fa fa-caret-down"></i>
                                    </button>
                                </div>
                            </div>
                            @if(isset($shifts))
                                <div class="col-sm-2">
                                    <select class="form-control" id="shift">
                                        @foreach($shifts as $shift)
                                            <option value="{{$shift->id}}">{{$shift->shift}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
                            @if(isset($locations))
                                <div class="col-sm-2">
                                    <select class="form-control" id="location">
                                        <option value="default" selected="selected">--- Select Location ---</option>
                                        @foreach($locations as $location)
                                            <option value="{{$location->id}}">{{$location->location}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
                            @if(isset($users))
                                <div class="col-sm-2">
                                    <select class="form-control" id="user">
                                        <option value="default" selected="selected">--- Select Agent ---</option>
                                        @foreach($users as $user)
                                            <option value="{{$user->id}}">{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
                            <div class="col-sm-2">
                                <select class="form-control" id="digit">
                                    <option value="2">2 Digits</option>
                                    <option value="3">3 Digits</option>
                                    <option value="4">4 Digits</option>
                                </select>
                            </div>
                            @if(isset($posts))
                                <div class="col-sm-2">
                                    <select class="form-control select2" multiple="multiple" data-placeholder="--- Select Posts ---" id="post">
                                        @foreach($posts as $key=>$post)
                                            <option value="{{$post}}">{{$post}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
                        </div>
                        <table id="table" class="table table-bordered table-hover dt-responsive">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Win Number</th>
                                <th>Bet Amount</th>
                                <th>Win Amount</th>
                                <th>Post</th>
                                <th>ticket</th>
                                <th>Shift</th>
                                <th>Dealer</th>
                                <th>Payee</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
@stop

@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.select2').select2();
            //Date range as a button
            $('#date-range span').html(moment().format('MMM D, YYYY') + ' - ' + moment().format('MMM D, YYYY'));
            $('#date-range').daterangepicker(
                {
                    autoApply: true,
                    ranges   : {
                        'Today'       : [moment(), moment()],
                        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment(),
                    endDate  : moment(),
                    maxDate: moment(),
                    //alwaysShowCalendars: true,
                    showCustomRangeLabel: true,
                    opens: "right"
                },
                function (start, end) {
                    $('#date-range span').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'))
                }
            );

            let table = fetch_data();

            $('#date-range, #shift, #location, #user, #digit, #post').on('change apply.daterangepicker', function(){

                if($(this).attr('id')=='location'){
                    $.ajax({
                        type: 'GET',
                        url: '{{ route('ajax-user') }}',
                        data: {location: $('#location').val()},
                        success: function (data) {
                            const option = new Option('--- Select Agent ---', 'default', true, true);
                            $('#user').html(option);
                            $.each(data, function (index) {
                                const newOption = new Option(data[index].agent_name+' ( '+data[index].agent_role+' )', data[index].agent_id, false, false);
                                $('#user').append(newOption);
                            });
                        }
                    });
                }

                $('#table').DataTable().destroy();
                table = fetch_data();
            });

            function fetch_data() {
                let post='';
                const posts = $('#post').val() != '' ? $('#post').val() : ['default'];
                posts.forEach(function (data,index, array) {
                    switch (data) {
                        case 'default':
                        case 'LO4P':
                            post += 'LO,A,B,C,D';
                            break;
                        case 'LOAB':
                            post += 'LO,A,B';
                            break;
                        case '4P':
                            post += 'A,B,C,D';
                            break;
                        default:
                            post += data + (index !=array.length-1 ? ',':'');
                            break;
                    }
                });

                // Data table for serverside
                const table = $('#table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax:{
                        url: "{{ route('winner.show') }}",
                        dataType: "json",
                        type: "GET",
                        data:{
                            _token: "{{ csrf_token() }}",
                            route:'winner',
                            date_range: $('#date-range span').html(),
                            shift: $('#shift').val(),
                            user: $('#user').val(),
                            digit: $('#digit').val(),
                            location: $('#location').val(),
                            post: post
                        }
                    },
                    columns: [
                        { data: "no" },
                        { data: "win_number" },
                        { data: "bet_amount" },
                        { data: "win_amount" },
                        { data: "post" },
                        { data: "ticket" },
                        { data: "shift" },
                        { data: "dealer" },
                        { data: "payee" },
                        { data: "status" },
                    ]

                });
                return table;
            }

            $('#getWinner').click(function () {
                $.ajax({
                    type:'POST',
                    url:"{{ route('winner.get') }}",
                    data:{ _token: "{{ csrf_token() }}",route:'winner.get'},
                    success:function(data) {
                        $("#winnerModal .modal-body").html('<p>'+data.msg+'</p>');
                    }
                });
            });
        });
    </script>
@endsection
