@extends('layouts.admin-master')

@section('title', $view_name)

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Bet
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Bet</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">

                        <!-- Filter Shift and Post Category -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <button type="button" class="btn btn-default btn-block" id="date-range">
                                        <i class="fa fa-calendar"></i>&nbsp;
                                        <span></span>
                                        &nbsp;<i class="fa fa-caret-down"></i>
                                    </button>
                                </div>
                            </div>
                            @if(isset($shifts))
                                <div class="col-sm-2">
                                    <select class="form-control" id="shift">
                                        @foreach($shifts as $shift)
                                            <option value="{{$shift->id}}">{{$shift->shift}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
                            @if(isset($locations))
                                <div class="col-sm-2">
                                    <select class="form-control" id="location">
                                        <option value="default" selected="selected">--- Select Location ---</option>
                                            @foreach($locations as $location)
                                                <option value="{{$location->id}}">{{$location->location}}</option>
                                            @endforeach
                                    </select>
                                </div>
                            @endif
                            @if(isset($users))
                                <div class="col-sm-2">
                                    <select class="form-control" id="user">
                                        <option value="default" selected="selected">--- Select Agent ---</option>
                                            @foreach($users as $user)
                                                <option value="{{$user->id}}">{{$user->name}}</option>
                                            @endforeach
                                    </select>
                                </div>
                            @endif
                            <div class="col-sm-2">
                                <select class="form-control" id="digit">
                                    <option value="2">2 Digits</option>
                                    <option value="3">3 Digits</option>
                                    <option value="4">4 Digits</option>
                                </select>
                            </div>
                            @if(isset($posts))
                                <div class="col-sm-2">
                                    <select class="form-control select2" multiple="multiple" data-placeholder="--- Select Posts ---" id="post">
                                            @foreach($posts as $key=>$post)
                                                <option value="{{$post->post}}">{{$post->post}}</option>
                                            @endforeach
                                    </select>
                                </div>
                            @endif
                        </div>
                        <table id="table" class="table table-bordered table-hover dt-responsive">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Bet Number</th>
                                <th>Sub Total</th>
                            </tr>
                            </thead>
                        </table>
                        <div class="col-sm-3 pull-right">
                            <div class="table-responsive">
                                <table class="table">
                                    <tr id="total">
                                        <th>Grand Total:</th>
                                        <td>0</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
@stop

@section('script')
    <script type="text/javascript">
        /* Custom filtering function which will search data in column four between two values */

        $(document).ready(function(){

            $('.select2').select2();
            //Date range as a button
            $('#date-range span').html(moment().format('MMM D, YYYY') + ' - ' + moment().format('MMM D, YYYY'));
            $('#date-range').daterangepicker(
                {
                    autoApply: true,
                    ranges   : {
                        'Today'       : [moment(), moment()],
                        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment(),
                    endDate  : moment(),
                    maxDate: moment(),
                    //alwaysShowCalendars: true,
                    showCustomRangeLabel: true,
                    opens: "right"
                },
                function (start, end) {
                    $('#date-range span').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'))
                }
            );

            let table = fetch_data();

            $('#date-range, #shift, #location, #user, #digit, #post').on('change apply.daterangepicker', function(){

                if($(this).attr('id')=='location'){
                    $.ajax({
                        type: 'GET',
                        url: '{{ route('ajax-user') }}',
                        data: {location: $('#location').val()},
                        success: function (data) {
                            const option = new Option('--- Select Agent ---', 'default', true, true);
                            $('#user').html(option);
                            $.each(data, function (index) {
                                const newOption = new Option(data[index].agent_name+' ( '+data[index].agent_role+' )', data[index].agent_id, false, false);
                                $('#user').append(newOption);
                            });
                        }
                    });
                }

                $('#table').DataTable().destroy();
                table = fetch_data();
            });

            // Add event listener for opening and closing details
            $('#table').on('click', 'td.details-control', function () {
                const tr = $(this).closest('tr');
                const row = table.row(tr);

                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                } else {
                    // Open this row
                    row.child(format(row.data().bet_data)).show();
                    tr.addClass('shown');
                }
            });

            function format ( dataSource ) {
                let html = '';
                html +='<table class="table table-striped table-bordered table-hover dt-responsive dataTable dtr-inline">'+
                    '<thead>' +
                    '<tr>' +
                    '<th>Dealer</th>' +
                    '<th>Post</th>' +
                    '<th>Bet Number</th>' +
                    '<th>Amount</th>' +
                    '<th>Ticket</th>' +
                    '<th>Time</th>' +
                    '<th>Date</th>' +
                    '</tr>' +
                    '</thead>';
                dataSource.forEach(function (item) {
                    html += '<tr>'+
                        '<td>' + item.dealer +'</td>'+
                        '<td>' + item.post +'</td>'+
                        '<td>' + item.bet_num +'</td>'+
                        '<td>' + item.amount +'</td>'+
                        '<td>' + item.ticket +'</td>'+
                        '<td>' + item.time +'</td>'+
                        '<td>' + item.date +'</td>'+
                        '</tr>';
                });
                return html += '</table>';
            }

            function fetch_data() {
                //let post='';
                const posts = $('#post').val() != '' ? $('#post').val() : null;
                /*posts.forEach(function (data,index, array) {
                    switch (data) {
                        case 'default':
                        case 'LO4P':
                            post += 'LO,A,B,C,D';
                            break;
                        case 'LOAB':
                            post += 'LO,A,B';
                            break;
                        case '4P':
                            post += 'A,B,C,D';
                            break;
                        default:
                            post += data + (index !=array.length-1 ? ',':'');
                            break;
                    }
                });*/
                //console.log($('#post').val()+', '+post)

                // Data table for serverside
                const table = $('#table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax:{
                        url: "{{ route('bet.show') }}",
                        dataType: "json",
                        type: "POST",
                        data:{
                            _token: "{{ csrf_token() }}",
                            route:'bet',
                            date_range: $('#date-range span').html(),
                            shift: $('#shift').val(),
                            user: $('#user').val(),
                            digit: $('#digit').val(),
                            location: $('#location').val(),
                            post: posts
                        }
                    },
                    columns: [
                        {className: 'details-control', orderable: false, data: null, defaultContent: ''},
                        { data: "bet_number" },
                        { data: "sub_total" },

                    ],
                    columnDefs: [
                        {searchable: false, orderable: false, targets: 0},
                        {className: 'all', targets: [1,2] }
                    ],
                    order: [[ 1, 'asc' ]],
                    pageLength: 100,

                    drawCallback : function ( row, data, start, end, display ) {
                        var api = this.api(), data;

                        // converting to interger to find total
                        const intVal = function (i) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '') * 1 :
                                typeof i === 'number' ?
                                    i : 0;
                        };

                        const Total = api
                            .column(2)
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);


                        // Update footer by showing the total with the reference of the column index
                        $('#total td').html(Intl.NumberFormat('en-US').format(Total));
                        //$('#total span').html(Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(Total));
                    },
                } );

                return table;
            }
        });
    </script>
@endsection
