@extends('layouts.admin-master')

@section('title', $view_name)

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Result
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Result</li>
        </ol>
    </section>
    <!-- Modal -->
    <div class="modal fade" id="resultModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Message</h4>
                </div>
                <div class="modal-body">
                    <div id='loading'>
                        <img class="img-fluid" src="{{ asset("images/loading.gif?v=".$version) }}"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="reload_page()" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


    <div class="box-header">
        <div class="row">
            <div class="hidden-print with-border col-sm-2">
                @if(auth()->user()->hasRole('Admin'))
                    <a href="#" id="getResult" class="btn btn-primary" data-toggle="modal" data-target="#resultModal" data-style="zoom-in">
                        <span><i class="fas fa-sync"></i> Get Result</span>
                    </a>
                @endif
                <button class="btn btn-default border-radius" onclick="reload_page()"><i class="fas fa-redo"></i> Reload</button>
            </div>
            <div class="col-sm-2">
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="datepicker">
                </div>
            </div>
        </div>
    </div>


    <!-- Main content -->
    <section class="content" id="divTab">

    </section>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            /** Date Picker **/
            $('#datepicker').datepicker({
                autoApply: true,
                format: "yyyy-mm-dd",
                endDate: "0"
            }).datepicker("setDate", "0");

            const date = $("#datepicker").val();
            fetch_data(date);

            $('#getResult').click(function () {
                $.ajax({
                    type:'POST',
                    url:"{{ route('result.get') }}",
                    data:{ _token: "{{ csrf_token() }}",route:'result.get'},
                    success:function(data) {
                        $("#resultModal .modal-body").html('<p>'+data.msg+'</p>');
                    }
                });
            });

            $("#datepicker").on("change",function(e) {
                //const shift = $("#shift").val();
                const date = $("#datepicker").val();
                fetch_data(date);
                e.preventDefault();
            });

            function fetch_data(date){
                $.ajax({
                    type: 'GET',
                    url: '{{ route('result.show') }}',
                    data: {date: date},
                    success: function (data) {
                        if(data.results != null) {
                            $('#divTab').html(data.results);
                        }
                        else{
                            $('#divTab').html(data.msg);
                        }
                    }

                });
            }
        });
    </script>
@endsection
