@extends('layouts.admin-master')

@section('title', $view_name)

@section('css')
    <!-- page -->
    <link rel="stylesheet" href="{{ asset('assets/dist/css/profile.css?v='.$version) }}">
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            User Profile
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fas fa-tachometer-alt"></i> Home</a></li>
            <li><a href="{{ route('profile') }}">User Profile</a></li>
            <li class="active">Update Account Info</li>
        </ol>
    </section>

    <div class="box-header">
        <div class="hidden-print with-border">
            <button class="btn btn-default border-radius" onclick="reload_page()"><i class="fas fa-redo"></i> Reload</button>
        </div>
    </div>
    <!-- /.box-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3">
                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" src="/assets/dist/img/user-profile-80x80.png" alt="User profile picture">

                        <h3 class="profile-username text-center">{{ $profile->name }}</h3>
                        <p class="text-muted text-center">{{ $profile->roles[0]->name }}</p>

                        <ul class="nav nav-tabs">
                            <li class="{{ empty(session('activeTab')) || session('activeTab')=='tab-profile' ? 'active' : '' }}"><a data-toggle="tab" href="#tab-profile" class="btn border-radius btn-block">Update Account Info</a></li>
                            <li class="{{ !empty(session('activeTab')) && session('activeTab')=='tab-password' ? 'active' : '' }}"><a data-toggle="tab" href="#tab-password" class="btn border-radius btn-block">Change Password</a></li>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <div class="col-md-9">
                <div class="box box-primary">
                    <!-- Bootstrap Tab -->
                    <div class="tab-content">
                        <div id="tab-profile" class="tab-pane fade{{ empty(session('activeTab')) || session('activeTab')=='tab-profile' ? ' in active ' : '' }}">
                            <!-- form start -->
                            <form method="POST" action="{{ route('profile') }}" role="form" id="frm-profile">
                                {{ csrf_field() }}
                                <div class="box-body">
                                    <div class="form-group required has-feedback{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label class="control-label" for="input-name">Name</label>
                                        <input type="text" class="form-control" id="input-name" name="name" placeholder="Enter name" value="{{ $profile->name }}" required>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group required has-feedback{{ $errors->has('username') ? ' has-error' : '' }}">
                                        <label class="control-label" for="input-username">Username</label>
                                        <input type="text" class="form-control" id="input-username" name="username" placeholder="Username" value="{{ $profile->username }}" required>
                                        @if ($errors->has('username'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('username') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <button type="submit" id="submit-save" class="btn btn-primary"><span class="fa fa-save" role="presentation" aria-hidden="true"></span> Save</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="reload_page()"><span class="fa fa-ban"></span> Close</button>
                                </div>
                            </form>
                        </div>
                        <div id="tab-password" class="tab-pane fade{{ !empty(session('activeTab')) && session('activeTab')=='tab-password' ? ' in active ' : '' }}">
                            <!-- form change password -->
                            <form method="POST" action="{{ route('profile.change-password') }}" role="form" id="frm-password">
                                {{ csrf_field() }}
                                <div class="box-body">
                                    <div class="form-group required has-feedback{{ $errors->has('current_password') ? ' has-error' : '' }}">
                                        <label class="control-label" for="input-current-password">Current password</label>
                                        <input type="password" class="form-control" id="input-current-password" name="current_password" placeholder="Current password" required>
                                        @if ($errors->has('current_password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('current_password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group required has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label class="control-label" for="input-password">New password</label>
                                        <input type="password" class="form-control" id="input-password" name="password" placeholder="New password" required>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group required has-feedback">
                                        <label class="control-label" for="input-password-confirm">Confirm password</label>
                                        <input type="password" class="form-control" id="input-password-confirm" name="password_confirmation" placeholder="Confirm password" required>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <button type="submit" id="submit-save" class="btn btn-primary"><span class="fa fa-save" role="presentation" aria-hidden="true"></span> Save</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="reload_page()"><span class="fa fa-ban"></span> Close</button>
                                </div>
                                <input type="hidden" name="hidden_activeTab" id="hidden-activeTab" value="{{ !empty(session('activeTab')) ? session('activeTab') : '' }}">
                            </form>
                        </div>
                    </div>
                    <!--- /.tab-content -->
                </div>
            </div>

        </div>
    </section>
@stop

@section('script')
    <!-- script page -->
    <script type="text/javascript">
        $(document).ready(function () {
            // check update form
            $("form#frm-profile, form#frm-password").each(function() {
                $(this).data('serialized', $(this).serialize());
            }).on('change input', function() {
                $(this).find('input:submit, button:submit').attr('disabled', $(this).serialize() == $(this).data('serialized'));
            }).find('input:submit, button:submit').attr('disabled', true);

            // current active tab
            $('.nav-tabs a').on('shown.bs.tab', function(event){
                var active_tab = $(event.target).attr("href");
                active_tab = active_tab.split("#");
                $("#hidden-activeTab").val(active_tab[1]);
            });

        });

    </script>
@endsection
