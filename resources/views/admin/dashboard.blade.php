@extends('layouts.admin-master')

@section('title', $view_name)

@section('css')
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('assets/bower_components/Ionicons/css/ionicons.min.css?v='.$version) }}">
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            {{--<small>Version 2.0</small>--}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            @if(isset($users))
                @php
                    $user_master=null;
                    if(auth()->user()->hasRole('Admin')){
                        $user_master = ($users->filter(function ($user){
                            return $user->roles()->where('role_id',2)->count();
                        }));
                    }

                    $user_dealer = ($users->filter(function ($user){
                        return $user->roles()->where('role_id',4)->count();
                    }));
                @endphp
                @if(isset($user_master))
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-aqua"><i class="fas fa-users"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Master</span>
                                <span class="info-box-number">{{count($user_master)}}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                @endif
                @if(isset($user_dealer))
                    <!-- /.col -->
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-yellow"><i class="fas fa-users"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Dealer</span>
                                <span class="info-box-number">{{count($user_dealer)}}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                @endif
            @endif
        </div>
        <!-- /.row -->

        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-md-6">
                <!-- TABLE: LATEST ORDERS -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Most Turnover</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="table-bet" class="table table-bordered table-hover dt-responsive">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>2 Digit</th>
                                    <th>3 Digit</th>
                                    <th>4 Digit</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        {{--<a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>--}}
                        <a href="{{ route('bet') }}" class="btn btn-sm btn-default btn-flat pull-right">View All Bet</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->

                <!-- DONUT CHART -->
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Most Bet by Location Chart</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <canvas id="pieChart" style="height:250px"></canvas>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->

            <div class="col-md-6">
                <!-- TABLE: LATEST ORDERS -->
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Most Win number</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="table-lose" class="table table-bordered table-hover dt-responsive">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Number</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <a href="{{ route('winner') }}" class="btn btn-sm btn-default btn-flat pull-right">View All</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->

                <!-- BAR CHART -->
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Bar Chart</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="chart">
                            <canvas id="barChart" style="height:230px"></canvas>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->

        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@stop

@section('script')
    <!-- ChartJS -->
    <script src="{{ asset('assets/bower_components/chart.js/Chart.js') }}"></script>

    <script type="text/javascript">
        /* Custom filtering function which will search data in column four between two values */

        $(document).ready(function(){

            $('#table-bet').DataTable({
                processing: true,
                serverSide: true,
                ajax:{
                    url: "{{ route('dashboard.show-betting') }}",
                    dataType: "json",
                    type: "GET",
                    data:{
                        _token: "{{ csrf_token() }}",
                        route:'dashboard',
                    }
                },
                columns: [
                    { data: "no"},
                    { data: "2digit" },
                    { data: "3digit" },
                    { data: "4digit" },

                ],
                ordering: false,
                searching: false,
                paging: false,
                info: false,
            });

            $('#table-lose').DataTable({
                processing: true,
                serverSide: true,
                ajax:{
                    url: "{{ route('dashboard.show-lose') }}",
                    dataType: "json",
                    type: "GET",
                    data:{
                        _token: "{{ csrf_token() }}",
                        route:'dashboard',
                    }
                },
                columns: [
                    { data: "no"},
                    { data: "number" },
                    { data: "total" },

                ],
                ordering: false,
                searching: false,
                paging: false,
                info: false,
            });

            $.ajax({
                url: "{{ route('dashboard.show-data') }}",
                type: "GET",
                data:{
                    _token: "{{ csrf_token() }}",
                    route:'dashboard',
                },
                dataType: 'json',
                success: function(data){

                    const chartData = {
                        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
                        datasets: [
                            {
                                label: 'Electronics',
                                fillColor: 'rgba(210, 214, 222, 1)',
                                strokeColor: 'rgba(210, 214, 222, 1)',
                                pointColor: 'rgba(210, 214, 222, 1)',
                                pointStrokeColor: '#c1c7d1',
                                pointHighlightFill: '#fff',
                                pointHighlightStroke: 'rgba(220,220,220,1)',
                                data: [65, 59, 80, 81, 56, 55, 40]
                            },
                            {
                                label: 'Digital Goods',
                                fillColor: 'rgba(60,141,188,0.9)',
                                strokeColor: 'rgba(60,141,188,0.8)',
                                pointColor: '#3b8bba',
                                pointStrokeColor: 'rgba(60,141,188,1)',
                                pointHighlightFill: '#fff',
                                pointHighlightStroke: 'rgba(60,141,188,1)',
                                data: [28, 48, 40, 19, 86, 27, 90]
                            }
                        ]
                    };
                    donutChart('#pieChart',data.location_bet);
                    barChart('#barChart',chartData);
                }
            });


            function donutChart(id,chartData) {
                // Get context with jQuery - using jQuery's .get() method.
                const pieChartCanvas = $(id).get(0).getContext('2d');
                const pieChart = new Chart(pieChartCanvas);
                const pieOptions = {
                    //Boolean - Whether we should show a stroke on each segment
                    segmentShowStroke: true,
                    //String - The colour of each segment stroke
                    segmentStrokeColor: '#fff',
                    //Number - The width of each segment stroke
                    segmentStrokeWidth: 2,
                    //Number - The percentage of the chart that we cut out of the middle
                    percentageInnerCutout: 50, // This is 0 for Pie charts
                    //Number - Amount of animation steps
                    animationSteps: 100,
                    //String - Animation easing effect
                    animationEasing: 'easeOutBounce',
                    //Boolean - Whether we animate the rotation of the Doughnut
                    animateRotate: true,
                    //Boolean - Whether we animate scaling the Doughnut from the centre
                    animateScale: false,
                    //Boolean - whether to make the chart responsive to window resizing
                    responsive: true,
                    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                    maintainAspectRatio: true,
                    //String - A legend template
                    legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
                };
                //Create pie or douhnut chart
                // You can switch between pie and douhnut using the method below.
                pieChart.Doughnut(chartData, pieOptions);
            }

            function barChart(id,chartData) {
                const barChartCanvas = $(id).get(0).getContext('2d');
                const barChart = new Chart(barChartCanvas);

                chartData.datasets[1].fillColor   = '#00a65a'
                chartData.datasets[1].strokeColor = '#00a65a'
                chartData.datasets[1].pointColor  = '#00a65a'
                const barChartOptions = {
                    //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
                    scaleBeginAtZero: true,
                    //Boolean - Whether grid lines are shown across the chart
                    scaleShowGridLines: true,
                    //String - Colour of the grid lines
                    scaleGridLineColor: 'rgba(0,0,0,.05)',
                    //Number - Width of the grid lines
                    scaleGridLineWidth: 1,
                    //Boolean - Whether to show horizontal lines (except X axis)
                    scaleShowHorizontalLines: true,
                    //Boolean - Whether to show vertical lines (except Y axis)
                    scaleShowVerticalLines: true,
                    //Boolean - If there is a stroke on each bar
                    barShowStroke: true,
                    //Number - Pixel width of the bar stroke
                    barStrokeWidth: 2,
                    //Number - Spacing between each of the X value sets
                    barValueSpacing: 5,
                    //Number - Spacing between data sets within X values
                    barDatasetSpacing: 1,
                    //String - A legend template
                    legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
                    //Boolean - whether to make the chart responsive
                    responsive: true,
                    maintainAspectRatio: true
                };

                barChartOptions.datasetFill = false
                barChart.Bar(chartData, barChartOptions)
            }
        });
    </script>
@endsection
