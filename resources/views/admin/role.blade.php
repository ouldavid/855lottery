@extends('layouts.admin-master')

@section('title', $view_name)

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Roles
            <small>advanced tables</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fas fa-tachometer-alt"></i> Home</a></li>
            <li><a href="{{ route('role') }}">Roles</a></li>
            <li class="active">List</li>
        </ol>
    </section>

    <div class="box-header">
        <div class="hidden-print with-border">
            <a href="#" class="btn btn-primary ladda-button" data-toggle="modal" data-target="#myModal" data-style="zoom-in">
            <span class="ladda-label"><i class="fa fa-plus"></i> Add Role</span></a>
            <button class="btn btn-default border-radius" onclick="reload_page()"><i class="fas fa-redo"></i> Reload</button>
        </div>
    </div>
    <!-- /.box-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">

                    <div class="box-body">
                        <table id="table-role" class="table table-bordered table-hover dt-responsive">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th style="max-width: 200px">Actions</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th style="max-width: 200px">Actions</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="reload_page()">&times;</button>
                    <h3 class="modal-title" id="myModalLabel">Add Role</h3>
                </div>

                <form method="POST" action="{{ route('role') }}" role="form" id="myForm">
                    {{ csrf_field() }}
                    <div class="modal-body" style="overflow: hidden;">
                        <div class="box-body">
                            <div class="form-group has-feedback">
                                <label for="input-name">Name</label>
                                <input type="text" class="form-control" id="input-name" placeholder="Enter name" name="name" value="" autofocus >
                                <span class="text-danger">
                                    <strong id="name-error"></strong>
                                </span>
                            </div>
                            <div class="form-group">
                                <label for="input-description">Description</label>
                                <input type="text" class="form-control" id="input-description" placeholder="Enter description" name="description" value="" >
                            </div>
                        </div>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="reload_page()"><span class="fa fa-ban"></span> Close</button>
                    <span id="submitFrm">
                        <input type="hidden" name="hidden_id" value="">
                        <button type="button" id="submit-save" class="btn btn-primary"><span class="fa fa-save" role="presentation" aria-hidden="true"></span> Save</button>
                        <button type="button" id="submit-update" class="btn btn-primary hide"><span class="fa fa-edit" role="presentation" aria-hidden="true"></span> Update</button>
                    </span>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@stop

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            // autofocus on modal
            $(document).on('shown.bs.modal', '.modal', function () {
                $(this).find('[autofocus]').focus();
            });

            // validation and save form
            $('body').on('click', '#submit-save', function(e) {
                e.preventDefault();
                var myForm = $("#myForm");
                var formData = myForm.serialize();
                $('#name-error').html("");

                $.ajax({
                    url: "{{ route('role') }}",
                    type: 'POST',
                    data: formData,
                    success:function(data) {
                        console.log(data);
                        if(data.errors) {
                            if(data.errors.name){
                                $('#name-error').html( data.errors.name[0] );
                            }
                        } else {
                            // reload page
                            reload_page();
                        }
                    },
                })
            });

            // update, send data to input
            $(document).on("click", "#dataEdit", function() {
                var id=$(this).data('id').id;
                var name=$(this).data('id').name;
                var description=$(this).data('id').description;
                $(".modal-title").text('Edit Role');
                $(".modal-body #input-name").val(name);
                $(".modal-body #input-description").val(description);
                $("#submit-save").addClass('hide');
                $("#submit-update").removeClass('hide');
                $("input[name=hidden_id]").val(id);
            });

            // update, submit data
            $('body').on('click', '#submit-update', function(e) {
                e.preventDefault();
                var formData = $("#myForm").serialize();
                var id = $("input[name=hidden_id]").val();
                $('#name-error').html("");

                $.ajax({
                    url: "/admin/role/update/"+id,
                    type: 'POST',
                    data: formData,
                    success:function(data) {
                        console.log(data);
                        if(data.errors) {
                            if(data.errors.name){
                                $('#name-error').html( data.errors.name[0] );
                            }
                        } else {
                            // data.success
                            reload_page();
                        }
                    },
                })
            });

        });

        // dataTables
        $(function () {
            $('#table-role').DataTable({
                searching: true,
                ordering: false,
                processing: true,
                serverSide: true,
                language: {
                    processing: '<img src="{{ asset("images/loading.gif?v=".$version) }}" />'
                    //processing: '<i class="fas fa-spinner fa-pulse" style="font-size: 3em;"></i>'
                },
                ajax:{
                    url: "{{ route('role.show') }}",
                    dataType: "json",
                    type: "POST",
                    data:{ _token: "{{ csrf_token() }}",route:'role'}
                },
                columns: [
                    { data: "id" },
                    { data: "name" },
                    { data: "description" },
                    { data: "actions" },
                ],
                aoColumnDefs: [
                    { "bSearchable": false, "aTargets": [0,2,3] }
                ],
            })
        });

        // delete data by id
        function delete_data(id,name) {
            if(confirm('Are you sure delete this data "'+name+'"?')) {
                $.ajax({
                    url: "/admin/role/destroy/"+id,
                    type: 'DELETE',
                    data:{ _token: "{{ csrf_token() }}",route:'role'},
                    success: function() {
                        // reload page
                        reload_page();
                    }
                });
            }else {
                return false;
            }
        }
    </script>
@endsection
