@extends('layouts.admin-master')

@section('title', $view_name)

@section('css')
    <!-- bootstrap toggle -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-toggle/bootstrap-toggle.min.css?v='.$version) }}">
@stop

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Settings
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Settings</li>
        </ol>
    </section>
    <!-- Modal -->

    <section class="content">
        <div class="row">
            @if(auth()->user()->hasRole('Admin'))
                <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Currency</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="currency_setting" class="table table-bordered">
                            <tr>
                                <th>Location</th>
                                <th>Currencies</th>

                            </tr>
                            @if(isset($locations))
                                @foreach($locations as $location)
                                    <tr>
                                        <td>{{$location->location}}</td>
                                        <td>
                                            <div class="btn-group" data-toggle="buttons">

                                                {{--<label class="btn btn-success active">
                                                    <input type="radio" name="options" id="option2" autocomplete="off" chacked>
                                                    $
                                                </label>--}}

                                                @foreach($currencies as $currency)
                                                    <label class='btn btn-default {{$location->currency_id == $currency->id ? "active":"" }}'>
                                                        <input type="radio" name="{{$location->id}}" id="option2" autocomplete="off" value="{{$currency->id}}">
                                                        <span>{{$currency->currency}}</span>
                                                    </label>
                                                @endforeach
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </section>
@stop

@section('script')
    <!-- bootstrap toggle -->
    <script src="{{ asset('assets/plugins/bootstrap-toggle/bootstrap-toggle.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".btn-group input").change(function(e) {
                const currency = $(this).prop("checked", true).val(),
                    location= $(this).attr('name');
                $.ajax({
                    type: 'GET',
                    url: '{{ route('setting.currency') }}',
                    data: {
                        currency_id: currency,
                        location_id: location
                    },
                    success: function (data) {}

                });
            });

            /*function post_setting(post,shift,setting){
                const
                    postLO =$("#post_setting input[data-post='1'][data-shift='"+shift+"']"),
                    postA= $("#post_setting input[data-post='2'][data-shift='"+shift+"']"),
                    postB= $("#post_setting input[data-post='3'][data-shift='"+shift+"']");
                switch(post){
                    case '1':
                        if(setting==true){
                            postA.bootstrapToggle('on');
                            postB.bootstrapToggle('on');
                        }
                        break;
                    case '2':
                    case '3':
                            postLO.bootstrapToggle(postA.prop('checked') == true && postB.prop('checked') == true ? 'on':'off');
                        break;
                }
            }*/
        });
    </script>
@endsection
