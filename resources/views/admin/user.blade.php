@extends('layouts.admin-master')

@section('title', $view_name)

@section('css')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css?v='.$version) }}">
    <!-- page -->
    <link rel="stylesheet" href="{{ asset('assets/dist/css/user.css?v='.$version) }}">
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Users
            <small>advanced tables</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fas fa-tachometer-alt"></i> Home</a></li>
            <li><a href="{{ route('user') }}">Users</a></li>
            <li class="active">List</li>
        </ol>
    </section>

    <div class="box-header">
        <div class="hidden-print with-border">
            <a href="#" class="btn btn-primary ladda-button" data-toggle="modal" data-target="#myModal" data-style="zoom-in">
                <span class="ladda-label"><i class="fa fa-plus"></i> Add User</span></a>
            <button class="btn btn-default border-radius" onclick="reload_page()"><i class="fas fa-redo"></i> Reload</button>
        </div>
    </div>
    <!-- /.box-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    {{--<div class="box-header">
                        <h3 class="box-title">Hover Data Table</h3>
                    </div>--}}
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="table-user" class="table table-bordered table-hover dt-responsive">
                            <thead>
                            <tr>
                                <th style="max-width:50px">No</th>
                                <th>Name</th>
                                <th>Username</th>
                                <th>Roles</th>
                                <th>Status</th>
                                <th>Balance</th>
                                <th style="max-width:210px">Setting</th>
                                <th style="max-width:120px">Actions</th>
                            </tr>
                            </thead>

                            <tbody></tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="reload_page()">&times;</button>
                    <h3 class="modal-title" id="myModalLabel">Add User</h3>
                </div>

                <form method="POST" action="{{ route('user') }}" role="form" id="myForm">
                    {{ csrf_field() }}
                    <div class="modal-body" style="overflow: hidden;">
                        <div class="box-body">
                            <div class="form-group has-feedback required">
                                <label class="control-label" for="input-name">Name</label>
                                <input type="text" class="form-control" id="input-name" name="name" placeholder="Enter name" value="{{ old('name') }}" autocomplete="name" autofocus>
                                <span class="text-danger">
                                    <strong id="name-error"></strong>
                                </span>
                            </div>
                            <div class="form-group has-feedback required">
                                <label class="control-label" for="input-username">Username</label>
                                <input type="text" class="form-control" id="input-username" name="username" placeholder="Username" value="{{ old('username') }}" autocomplete="off" readonly
                                       onfocus="this.removeAttribute('readonly');">
                                <span class="text-danger">
                                    <strong id="username-error"></strong>
                                </span>
                            </div>
                            <div class="form-group has-feedback required">
                                <label class="control-label" for="input-password">Password</label>
                                <input type="password" class="form-control" id="input-password" name="password" placeholder="Password" autocomplete="off" readonly
                                       onfocus="this.removeAttribute('readonly');">
                                <span class="text-danger">
                                    <strong id="password-error"></strong>
                                </span>
                            </div>
                            <div class="form-group has-feedback required">
                                <label class="control-label" for="input-password-confirm">Confirm password</label>
                                <input type="password" class="form-control" id="input-password-confirm" name="password_confirmation" placeholder="Confirm password" autocomplete="off" readonly
                                       onfocus="this.removeAttribute('readonly');">
                            </div>

                            <div class="form-group">
                                <label>User role</label>
                                <select class="form-control" name="slt_role" id="slt-role">
                                    @foreach($roles as $role)
                                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @if(auth()->user()->hasRole('Admin'))
                            <div class="form-group has-feedback required">
                                <label class="control-label">User parent</label>
                                <select class="form-control select2" name="slt_parent" id="slt-parent" style="width: 100%;">
{{--                                    <option value="" selected>-- no select --</option>--}}
                                    @foreach($parent_accounts as $parent_account)
                                        <option value="{{ $parent_account->id }}">{{ $parent_account->name." (".$parent_account->roles[0]->name.")" }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger">
                                    <strong id="parent-error"></strong>
                                </span>
                            </div>
                            <div class="form-group has-feedback required">
                                <label class="control-label">Location</label>
                                <select class="form-control" name="slt_location" id="slt-location">
                                    <option value="" selected>-- no select --</option>
                                    @foreach($locations as $location)
                                        <option value="{{ $location->id }}">{{ $location->location }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger">
                                    <strong id="location-error"></strong>
                                </span>
                            </div>
                            @endif
                            <div class="form-group">
                                <label class="d-block">User status</label>
                                <div class="radio inline">
                                    <label>
                                        <input type="radio" name="is_activated" id="radio-1" value="1" checked="">
                                        Active
                                    </label>
                                </div>
                                <div class="radio inline">
                                    <label>
                                        <input type="radio" name="is_activated" id="radio-2" value="0">
                                        Inactive
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="reload_page()"><span class="fa fa-ban"></span> Close</button>
                        <span id="submitFrm">
                            <input type="hidden" name="hidden_id" value="">
                            <button type="button" id="submit-save" class="btn btn-primary"><span class="fa fa-save" role="presentation" aria-hidden="true"></span> Save</button>
                            <button type="button" id="submit-update" class="btn btn-primary hide"><span class="fa fa-edit" role="presentation" aria-hidden="true"></span> Update</button>
                        </span>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal user setting -->
    <div class="modal fade" id="myModal-setting" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-setting" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="reload_page()">&times;</button>
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#vn">Vietnam</a></li>
                        <li><a data-toggle="tab" href="#th">Thai</a></li>
                        <li><h3 class="modal-title" id="myModalLabel-setting">User Setting</h3></li>
                    </ul>
                </div>

                <form method="POST" action="{{ route('user.update-setting') }}" role="form" id="myForm-setting">
                    {{ csrf_field() }}
                    <div class="modal-body" style="overflow: hidden;">
                        <div class="box-body">
                            <div class="tab-content">

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="reload_page()"><span class="fa fa-ban"></span> Close</button>
                        <span id="submitFrm-setting">
                            <button type="submit" id="submit-update-setting" class="btn btn-primary"><span class="fa fa-edit" role="presentation" aria-hidden="true"></span> Update</button>
                        </span>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.Modal user setting -->

    <!-- Modal deposit -->
    <div class="modal fade" id="myModal-deposit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-deposit" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="reload_page()">&times;</button>
                    <h3 class="modal-title" id="myModalLabel-deposit">Deposit to <b id="deposit-name"></b></h3>
                </div>
                <form method="POST" action="" role="form" id="myForm-deposit">
                    {{ csrf_field() }}
                    <div class="modal-body" style="overflow: hidden;">
                        <div class="box-body">
                            <div class="form-group has-feedback required">
                                <label class="control-label" for="input-deposit">Balance</label>
                                <input type="text" class="form-control" id="input-deposit" name="deposit" placeholder="Enter balance" autocomplete="off" autofocus>
                                <span class="text-danger">
                                    <strong id="deposit-error"></strong>
                                </span>
                            </div>
                             <div class="form-group">
                                <label class="control-label" for="input-remark">Remark</label>
                                <input type="text" class="form-control" id="input-remark" name="remark" placeholder="Enter remark" value="">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="reload_page()"><span class="fa fa-ban"></span> Close</button>
                        <span id="submitFrm-deposit">
                            <button type="button" id="submit-deposit" class="btn btn-primary"><span class="fa fa-save" role="presentation" aria-hidden="true"></span> Save</button>
                        </span>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.Modal user deposit -->

    <!-- Modal withdraw -->
    <div class="modal fade" id="myModal-withdraw" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-withdraw" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="reload_page()">&times;</button>
                    <h3 class="modal-title" id="myModalLabel-deposit">Withdraw from <b id="withdraw-name"></b></h3>
                </div>
                <form method="POST" action="" role="form" id="myForm-withdraw">
                    {{ csrf_field() }}
                    <div class="modal-body" style="overflow: hidden;">
                        <div class="box-body">
                            <div class="form-group has-feedback required">
                                <label class="control-label" for="input-withdraw">Balance</label>
                                <input type="text" class="form-control" id="input-withdraw" name="withdraw" placeholder="Enter balance" autocomplete="off" autofocus>
                                <span class="text-danger">
                                    <strong id="withdraw-error"></strong>
                                </span>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-remark">Remark</label>
                                <input type="text" class="form-control" id="input-remark" name="remark" placeholder="Enter remark" value="">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="reload_page()"><span class="fa fa-ban"></span> Close</button>
                        <span id="submitFrm-withdraw">
                            <button type="button" id="submit-withdraw" class="btn btn-primary"><span class="fa fa-save" role="presentation" aria-hidden="true"></span> Save</button>
                        </span>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.Modal user withdraw -->
@stop

@section('script')
    <!-- Select2 -->
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <!-- script page -->
    <script type="text/javascript">
        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2();
            // Ajax user role
            $("select#slt-role").on('change', function() {
                var role_id = $(this).val();
                var user_id = $(this).attr('rel');
                $.ajax({
                    type: "GET",
                    url: "/admin/user/ajax-parent",
                    data: { role_id:role_id,user_id:user_id },
                    success:function(result) {
                        if(result.user_parents) {
                            $("#slt-location").html('<option value="" selected="">-- no select --</option>');
                            $('#slt-parent').html(result.user_parents);
                        }
                    }
                })
            });
            // Ajax user parent
            $("select#slt-parent").on('change', function() {
                var parents_id = $(this).val();
                var role_id = $("#slt-role").val();
                $.ajax({
                    type: "GET",
                    url: "/admin/user/ajax-parent",
                    data: { role_id:role_id, parents_id:parents_id },
                    success:function(result) {
                        if(result.location) {
                            $('#slt-location').html(result.location);
                        }
                    }
                })
            });

            // autofocus on modal
            $(document).on('shown.bs.modal', '.modal', function () {
                $(this).find('[autofocus]').focus();
            });

            // validation and save form
            $('body').on('click', '#submit-save', function(e) {
                e.preventDefault();
                var myForm = $("#myForm");
                var formData = myForm.serialize();
                $('.text-danger strong').html("");

                $.ajax({
                    url: "{{ route('user') }}",
                    type: 'POST',
                    data: formData,
                    success:function(data) {
                        if(data.errors) {
                            if(data.errors.name){
                                $('#name-error').html( data.errors.name[0] );
                            }
                            if(data.errors.username){
                                $('#username-error').html( data.errors.username[0] );
                            }
                            if(data.errors.password){
                                $('#password-error').html( data.errors.password[0] );
                            }
                            if(data.errors.slt_location){
                                $('#location-error').html( data.errors.slt_location[0] );
                            }
                            if(data.errors.slt_parent){
                                $('#parent-error').html( data.errors.slt_parent[0] );
                            }
                        }
                        if(data.success) {
                            // reload page
                            reload_page();
                        }
                    },
                })
            });

            // update, send data to input
            $(document).on("click", "a.data-edit", function() {
                var id = $(this).data('id').id;
                var name = $(this).data('id').name;
                var username = $(this).data('id').username;
                var user_role = $(this).data('id').user_role;
                var location = $(this).data('id').location;
                var user_parent = $(this).data('id').user_parent;
                var is_activated = $(this).data('id').is_activated;
                $(".modal-title").text('Edit User');
                // send value to input
                $(".modal-body #input-name").val(name);
                $(".modal-body #input-username").val(username);
                // user role
                $(".modal-body #slt-role").val(user_role);
                // add rel user id
                $(".modal-body #slt-role").attr('rel', id);
                $('.modal-body input[name="is_activated"][value="'+is_activated+'"]').prop('checked',true);
                // Ajax parent user
                $.ajax({
                    type: "GET",
                    url: "/admin/user/ajax-parent",
                    data: { role_id:user_role, user_id:id, edit_user:1 },
                    success:function(result) {
                        if(result.user_parents) {
                            $('#slt-parent').html(result.user_parents);
                            $('#slt-parent').val(user_parent);
                        }
                        if(result.location) {
                            $('#slt-location').html(result.location);
                            $('#slt-location').val(location);
                        }
                        if(result.user_role) {
                            $("#slt-role, #slt-parent, #slt-location").children('option').attr("disabled", true);
                            $("#slt-parent").children("option[value*='"+user_parent+"']").attr("disabled", false);
                            $("#slt-location").children("option[value*='"+location+"']").attr("disabled", false);
                        }
                    }
                });
                // change bnt submit
                $("#submit-save").addClass('hide');
                $("#submit-update").removeClass('hide');
                $("input[name=hidden_id]").val(id);
            });
            // update, submit data
            $('body').on('click', '#submit-update', function(e) {
                e.preventDefault();
                var formData = $("#myForm").serialize();
                var id = $("input[name=hidden_id]").val();
                $('.text-danger strong').html("");

                $.ajax({
                    url: "/admin/user/update/"+id,
                    type: 'POST',
                    data: formData,
                    success:function(data) {
                        console.log(data);
                        if(data.errors) {
                            if(data.errors.name){
                                $('#name-error').html( data.errors.name[0] );
                            }
                            if(data.errors.username){
                                $('#username-error').html( data.errors.username[0] );
                            }
                            if(data.errors.password){
                                $('#password-error').html( data.errors.password[0] );
                            }
                            if(data.errors.slt_location){
                                $('#location-error').html( data.errors.slt_location[0] );
                            }
                            if(data.errors.slt_parent){
                                $('#parent-error').html( data.errors.slt_parent[0] );
                            }
                        }
                        if(data.success) {
                            // reload page
                            reload_page();
                        }
                    },
                })
            });

            // user setting, load data
            $(document).on("click", ".data-setting", function() {
                var user_id = $(this).data('id').id;
                if(user_id) {
                    $.ajax({
                        type: "GET",
                        url: "/admin/user/ajax-setting/"+user_id,
                        success:function(result) {
                            if(result) {
                                $('#myForm-setting .tab-content').append(result);
                            }
                        }
                    })
                }
            });

        });

        // balance deposit, send data to input
        $(document).on("click", "a.data-deposit", function() {
            var deposit_name = $(this).data('id').name;
            var user_id = $(this).data('id').id;
            // send value
            $("b#deposit-name").text(deposit_name);
            $("#myForm-deposit").attr("action", "/admin/user/deposit/"+user_id);
            $.ajax({
                url: "/admin/user/deposit/"+user_id,
                type: 'POST',
                data:{ _token: "{{ csrf_token() }}"}
            })

        });
        // validation and save form balance deposit
        $('body').on('click', '#submit-deposit', function(e) {
            e.preventDefault();
            var myForm = $("#myForm-deposit");
            var formData = myForm.serialize();
            $('.text-danger strong').html("");
            // ajax send form data
            $.ajax({
                url: myForm.attr('action'),
                type: 'POST',
                data: formData,
                success:function(data) {
                    console.log(data);
                    if(data.errors) {
                        if(data.errors.deposit){
                            $('#deposit-error').html( data.errors.deposit[0] );
                        }
                    }
                    if(data.success) {
                        // reload page
                        reload_page();
                    }
                },
            })
        });
        // balance withdraw, send data to input
        $(document).on("click", "a.data-withdraw", function() {
            var withdraw_name = $(this).data('id').name;
            var user_id = $(this).data('id').id;
            // send value
            $("b#withdraw-name").text(withdraw_name);
            $("#myForm-withdraw").attr("action", "/admin/user/withdraw/"+user_id);
            $.ajax({
                url: "/admin/user/withdraw/"+user_id,
                type: 'POST',
                data:{ _token: "{{ csrf_token() }}"}
            })
        });
        // validation and save form balance deposit
        $('body').on('click', '#submit-withdraw', function(e) {
            e.preventDefault();
            var myForm = $("#myForm-withdraw");
            var formData = myForm.serialize();
            $('.text-danger strong').html("");
            // ajax send form data
            $.ajax({
                url: myForm.attr('action'),
                type: 'POST',
                data: formData,
                success:function(data) {
                    console.log(data);
                    if(data.errors) {
                        if(data.errors.withdraw){
                            $('#withdraw-error').html( data.errors.withdraw[0] );
                        }
                    }
                    if(data.success) {
                        // reload page
                        reload_page();
                    }
                },
            })
        });

        // Data Table
        $(function () {
            // data table
            $('#table-user').DataTable({
                aaSorting: [[0, 'asc']],
                processing: true,
                serverSide: true,
                language: {
                    processing: '<img src="{{ asset("images/loading.gif?v=".$version) }}" />'
                },
                ajax:{
                    url: "{{ route('user.show') }}",
                    dataType: "json",
                    type: "POST",
                    data:{ _token: "{{ csrf_token() }}",route:'user'}
                },
                columns: [
                    { data: "id" },
                    { data: "name" },
                    { data: "username" },
                    { data: "roles" },
                    { data: "is_activated" },
                    { data: "balance" },
                    { data: "setting" },
                    { data: "actions" },
                ],
                aoColumnDefs: [
                    {
                        "bSortable": false, "aTargets": [3,4,5,6],
                        //"bSearchable": false, "aTargets": [0,4,5]
                    }
                ],
            })

        });

        // delete data by id
        function delete_data(id,name) {
            if(confirm('Are you sure delete this data "'+name+'"?')) {
                $.ajax({
                    url: "/admin/user/destroy/"+id,
                    type: 'DELETE',
                    data:{ _token: "{{ csrf_token() }}",route:'user'},
                    success: function() {
                        // reload page
                       reload_page();
                    }
                });
            }else {
                return false;
            }
        }

        // reset password
        function reset_password(user_id,name) {
            if(confirm('Do you want to reset password this user "'+name+'"?\nDefault password (123456789)')) {
                $.ajax({
                    url: "/admin/user/reset-password/"+user_id,
                    type: 'POST',
                    data: { _token: "{{ csrf_token() }}"},
                    success: function(data) {
                        if(data.success) {
                            // reload page
                            reload_page();
                        }
                        if(data.warning) {
                            alert('This action is unauthorized.');
                            // reload page
                            reload_page();
                        }
                    }
                });
            }else {
                return false;
            }
        }


    </script>
@endsection
