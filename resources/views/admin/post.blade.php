@extends('layouts.admin-master')

@section('title', $view_name)

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Posts
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Posts</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">

                        <!-- Filter Shift and Post Category -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="table-post" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Shift</th>
                                <th>Location</th>
                                <th>Post</th>
                                <th>2D</th>
                                <th>3D</th>
                                <th>4D</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($posts as $post)
                                <tr>
                                    <td>{{$post->shift->shift}}</td>
                                    <td>{{$post->location_type->type}}</td>
                                    <td>{{$post->post->post}}</td>
                                    <td>{{$post->getAttribute("2D")}}</td>
                                    <td>{{$post->getAttribute("3D")}}</td>
                                    <td>{{$post->getAttribute("4D")}}</td>
                                    <td>Edit</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
@stop

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#table-post').DataTable( {
                initComplete: function () {
                    this.api().columns([0,1]).every( function () {
                        var column = this;
                        var select = $('<select></select>')
                            .appendTo( $('.box-header'))
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()

                                );
                                column
                                    .search( val ? '^'+val+'$' : '', true, false )
                                    .draw();
                            } );

                        column.data().unique().sort().each( function ( d ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );
                    } );
                }
            } );
        } );
    </script>
@endsection
