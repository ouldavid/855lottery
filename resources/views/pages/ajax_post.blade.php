@foreach($posts as $post)
    <li>
        <div class="outer circle shapeborder">
            <div class="inner circle shapeborder"><a href="#" rel="{{$post->post}}">{{$post->post}}</a></div>
        </div>
    </li>
@endforeach

<script type="text/javascript">
    $(document).ready(function () {
        $('#divPost li').click(function () {
            if($(this).hasClass('current')){
                if($('#divPost li.current').length > 1) {
                    if($(this).find('a').attr('rel').length > 1 || $('#divPost li.current').find('a').attr('rel').length > 1) {
                        $('#divPost li.current').addClass('current');
                    }
                    $(this).removeClass('current');
                }
            }
            else{
                if($(this).find('a').attr('rel').length > 1 || $('#divPost li.current').find('a').attr('rel').length > 1) {
                    $('#divPost li.current').removeClass('current');
                }

                $(this).addClass('current');
            }
        });
    });
</script>
