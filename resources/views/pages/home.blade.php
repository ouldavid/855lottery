@extends('layouts.admin-master')

@section('title', $view_name)

@section('css')
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css?v='.$version) }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css?v='.$version) }}">
    <!-- page -->
    <link rel="stylesheet" href="{{ asset('css/home.css?v='.$version) }}">
@endsection

@section('content')
    <input type="hidden" name="hidden_id" data-detail-id="0" data-delete-id="0" data-row-id="0" value="0">
    <!-- Modal Success or Fail -->
    <div class="modal fade" id="homeModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{trans("words.messages.message_title")}}</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("words.messages.close")}}</button>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal Lottery Type -->
    <div class="modal fade" id="lotteryModal" tabindex="-1" role="dialog" aria-labelledby="lotteryModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="lotteryModalTitle">{{trans("words.messages.message_title")}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>{{trans("words.messages.lottery_type")}}</p>
                </div>
                <div class="modal-footer">
                    <button id="btn-no" type="button" class="btn btn-secondary" data-dismiss="modal">{{trans("words.messages.no")}}</button>
                    <button id="btn-yes" type="button" class="btn btn-primary">{{trans("words.messages.yes")}}</button>
                </div>
            </div>
        </div>
    </div>

    <!--- table bet --->
    <div id="betTable">
        <div class="box">
            <div class="box-body no-padding">
                <table id="bet-list" class="table table-condensed" cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>
                        <th>{{ trans('words.post')}}</th>
                        <th>{{ trans('words.number')}}</th>
                        <th>{{ trans('words.amount')}}</th>
                        <th>{{ trans('words.sub_total')}}</th>
                        @if(time() < strtotime($closing_time->day))
                            @if(isset($provinces) && $provinces->count() > 1)
                                <th>{{ trans('words.province')}}</th>
                            @endif
                        @endif
                        <th>{{ trans('words.action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->


            <div class="box-footer clearfix">
                <div id="divAccount" class="pull-left">
                    <p>{{ trans('words.balance')}} : <span>{{number_format($user->account->balance, 0, '.', ',')}}</span>{{$user->location->currency->symbol}}</p>
                </div>
                <ul id="total" class="no-margin pull-right">
                    <li>{{ trans('words.total')}} : <span>0</span>{{$user->location->currency->symbol}}</li>
                    <li>{{ trans('words.grand_total')}}: <span>0</span>{{$user->location->currency->symbol}}</li>
                </ul>
            </div>
        </div>
    </div><!--- end #betTable --->

    <!-- post -->
    <div id="divPost">
        <ul>
            @foreach($posts as $post)
                <li>
                    <div class="outer circle shapeborder">
                        <div class="inner circle shapeborder"><a href="#" rel="{{$post->post}}">{{$post->post}}</a></div>
                    </div>
                </li>
            @endforeach
        </ul>

    </div><!--- end post --->

    <!--- calculator --->
    <div id="calculator" role="application">
        <!-- Screen -->
        <div class="top">
            <form role="form">
                <div class="row">
                    <div class="form-group col-xs-4">
                        <input type="text" class="form-control focus" maxlength="4" id="bet-number" placeholder="{{ trans('words.number')}}" readonly>
                    </div>
                    <div class="form-group col-xs-1">
                        <p id="bet-operator" data-operator=""></p>
                    </div>
                    <div class="form-group col-xs-3">
                        <input type="text" class="form-control" id="bet-type" required placeholder="{{ trans('words.option')}}" readonly disabled>
                    </div>
                    <div class="form-group col-xs-4">
                        <input type="text" class="form-control" id="bet-amount" placeholder="{{ trans('words.amount')}}" readonly disabled>
                    </div>
                </div>
            </form>
        </div>

        <div class="keys" aria-labelledby="inputKeys">
            <!-- operators and other keys -->
            <div class="group-buttons">
                <span tabindex="0" class="operator" id="btn-new">{{ trans('words.new_ticket')}}</span>
                <span tabindex="0" class="operator" id="btn-clear">{{ trans('words.delete')}}</span>
                <span tabindex="0" class="operator fa fa-backspace" id="btn-backspace"></span>
                <span tabindex="0" class="operator glyphicon glyphicon-arrow-left" id="btn-roll-middle" rel="←" data-operator="roll-middle"></span>
            </div>
            <div class="group-buttons">
                <span tabindex="0" rel="7">7</span>
                <span tabindex="0" rel="8">8</span>
                <span tabindex="0" rel="9">9</span>
                <span tabindex="0" class="operator glyphicon glyphicon-arrow-up" id="btn-roll-first" rel="↑" data-operator="roll-first"></span>
            </div>
            <div class="group-buttons">
                <span tabindex="0" rel="4">4</span>
                <span tabindex="0" rel="5">5</span>
                <span tabindex="0" rel="6">6</span>
                <span tabindex="0" class="operator glyphicon glyphicon glyphicon-arrow-down" id="btn-roll-last" rel="↓" data-operator="roll-last"></span>
            </div>
            <div class="group-buttons">
                <span tabindex="0" rel="1">1</span>
                <span tabindex="0" rel="2">2</span>
                <span tabindex="0" rel="3">3</span>
                <span tabindex="0" class="operator glyphicon glyphicon-remove" id="btn-box" rel="x" data-operator="box"></span>
            </div>
            <div class="group-buttons">
                <span tabindex="0" rel="0">0</span>
                <span tabindex="0" class="operator enter" id="btn-enter"><img src="{{ asset('images/icon-enter.png') }}" width="26" alt="Enter"></span>
                <span tabindex="0" class="operator col-blue" id="btn-bet" data-toggle="modal" data-target="#homeModal">{{ trans('words.bet')}}</span>
            </div>
        </div>
    </div>
@stop

@section('script')
    <!-- bootstrap datepicker -->
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <!-- script page -->
    <script type="text/javascript">
        $(document).ready(function () {

            /** Date Picker **/
            $('#datepicker').datepicker({
                autoClose: true,
                endDate: "0"
            }).datepicker("setDate", "0");

            /** Post **/
            //select Post A as Default
            $('#divPost li:first-child').addClass('current');
            //$('#divPost li a[rel=LOAB]').parents('li').addClass('lo4p');
            //on Post selected
            $('#divPost li').click(function () {
                if($(this).hasClass('current')){
                    if($('#divPost li.current').length > 1) {
                        if($(this).find('a').attr('rel').length > 1 || $('#divPost li.current').find('a').attr('rel').length > 1) {
                            $('#divPost li.current').addClass('current');
                        }
                        $(this).removeClass('current');
                    }
                }
                else{
                    if($(this).find('a').attr('rel').length > 1 || $('#divPost li.current').find('a').attr('rel').length > 1) {
                        $('#divPost li.current').removeClass('current');
                    }

                    $(this).addClass('current');
                }
            });

            /** Modal for lottery type**/
            let previous;
            const today = new Date();
            today.setHours(0,0,0,0);
            $("#lottery").on('focus', function () {
                previous = $(this).val();
            }).change(function() {
                const lottery = $("#lottery").val();
                const selectedDate = new Date($("#datepicker").val());

                if($('#bet-list tbody tr').length > 0 && today.getTime() == selectedDate.getTime()){
                    $("#lotteryModal").modal('show');
                }
                else{
                    ajaxTicket(today);
                }


                $.ajax({
                    type: 'GET',
                    url: '{{ route('ajax-post') }}',
                    data: {lottery: lottery},
                    success: function (data) {
                        $('#divPost ul').html(data);
                        $('#divPost li:first-child').addClass('current');
                    }
                });
            });

            $("#lotteryModal .modal-footer button").click(function (e) {
                if($(this).attr('id') == 'btn-no'){
                    $('#lottery option[value='+previous+']').prop('selected', true);
                }
                else{
                    $("#lotteryModal").modal('hide');
                    clearAfterBet();
                    ajaxTicket(today);
                }
            });

            /** Ajax for Betting list **/
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            /** Load data by Shift, Date and Ticket change **/
            $("#shift, #datepicker").on("change",function(e) {
                ajaxTicket(today);
                e.preventDefault();
            });


            /** Click Enter Button to add bet to show on table over **/
            $("#btn-enter").click(function(e){
                if($('#ticket').hasClass('focus')){
                    loadData();
                }
                else if($("#bet-number").val().length > 1 && $("#bet-amount").val() != '' && $( "#divPost li" ).hasClass( "current" )){
                    let post = '';
                    let bet_type = '';
                    let id = $('#bet-list tbody tr').length;
                    const operator = $('#bet-operator').attr('data-operator');
                    const operator_text = $('#bet-operator').text();
                    const bet_amount =
                        $("#bet-amount").val().charAt($("#bet-amount").val().length - 1) == '0' &&
                        $("#bet-amount").val().charAt($("#bet-amount").val().length - 2) == '0' ?
                            $("#bet-amount").val() : $("#bet-amount").val() + '00';

                    if (operator != '') {
                        bet_type = $("#bet-type").val();
                    }

                    $("#divPost li.current a").each(function (index) {
                        post += $(this).attr('rel');
                        if($(this).attr('rel').length == 1 && index != $("#divPost li.current").length-1)
                            post += ',';
                    });

                    const bet = {
                        shift: $('#shift').val(),
                        date : $("#datepicker").val(),
                        ticket : $("#ticket").val(),
                        bet_detail_id: $("input[name=hidden_id]").attr('data-detail-id') == 0 ? 0 : $("input[name=hidden_id]").attr('data-detail-id'),
                        id: $("input[name=hidden_id]").attr('data-row-id') == 0 ? ++id : $("input[name=hidden_id]").attr('data-row-id'),
                        post: post,
                        bet_number: $("input[id=bet-number]").val(),
                        operator: operator,
                        operator_text: operator_text,
                        bet_type: bet_type,
                        bet_amount: parseInt(bet_amount),
                        province: $("#province").val() == 'undefined'? false:$("#province").val(),
                        lottery: $("#lottery").val() == 'undefined'? false:$("#lottery").val()
                    };

                    $.ajax({
                        type: 'GET',
                        url: '{{ route('ajax-bet') }}',
                        data: bet,
                        success: function (data) {
                            console.log(data.lottery);
                            if(data.closing_time && data.lottery != '2'){
                                $("#homeModal").modal('show');
                                if(data.closing_time== 1){
                                    $("#shift option").removeAttr('selected');
                                    $("#homeModal .modal-body").html('<p class="text-danger">{{trans("words.messages.day_time_expired")}}</p>');
                                    $("#shift option[value!="+data.closing_time+"]").attr('selected',true);
                                }
                                else{
                                    $("#homeModal .modal-body").html('<p class="text-danger">{{trans("words.messages.night_time_expired")}}</p>');
                                }
                                clearAfterBet();
                            }
                            else{
                                const grand_total =
                                    parseInt($('#total li:last-child span').text()) +
                                    ((parseInt(data.sub_total) * parseInt(data.setting['discount']) / 100)),

                                    balance = parseInt($("#divAccount p span").text().replace(/[^0-9\.-]+/g, ""));
                                if( balance >= grand_total) {
                                    if ($("input[name=hidden_id]").attr('data-row-id') == 0) {
                                        //alert(data.sub_total+', '+data.setting['discount']+', '+data.setting['digit']);
                                        $("#bet-list tbody").prepend(data.bet_list);
                                        setTotal(data.sub_total, data.setting['discount'], data.setting['digit']);
                                    }
                                    else {
                                        $('#bet-list tbody tr').each(function (index) {
                                            if ($(this).attr('data-row-id') == $("input[name=hidden_id]").attr('data-row-id')) {
                                                $(this).after(data.bet_list);
                                                deleteEntry($(this).children());
                                            }
                                        });
                                        $("input[name=hidden_id]").attr({'data-row-id':'0','data-detail-id':'0','data-delete-id':'0'});
                                    }

                                    //Clear all input box and focus on bet number box
                                    $("#calculator input").val('');
                                    $("#bet-operator").text('');
                                    $('#bet-operator').attr('data-operator','');
                                    $("#bet-type").attr('disabled', 'disabled');
                                    $("#bet-number").focus();
                                    $("span.operator").addClass('isDisabled');
                                    $("#btn-bet").removeClass('isDisabled');
                                }
                                else{
                                    $("#homeModal").modal('show');
                                    $("#homeModal .modal-body").html('<p class="text-danger">{{trans("words.messages.not_enough_balance")}}</p>');
                                }
                            }
                        }
                    });
                }
                e.preventDefault();
            });
            /** Bet Button **/
            $("#btn-bet").click(function(e){
                const bet_data = [];
                $('#bet-list tbody tr').each(function(index, tr){
                    const bet_num = $(tr).find('td:eq(1)').text().split(' ');
                    bet_data.push({
                        bet_detail_id : $(tr).data("detail-id") !== undefined ? $(tr).data('detail-id') : 0,
                        post :$(tr).find('td:eq(0)').text(),
                        bet_number : bet_num[0],
                        operator : bet_num.length > 1  ? $(tr).find('td:eq(1)').attr('data-operator') : 'default',
                        bet_type : bet_num.length > 1  ? bet_num[2] : '',
                        bet_amount : parseInt($(tr).find('td:eq(2)').text().replace(/[^0-9\.-]+/g,"")),
                        sub_total : parseInt($(tr).find('td:eq(3)').text().replace(/[^0-9\.-]+/g,"")),
                        province : $(tr).find('td:eq(4)').children().length <1 ? $(tr).find('td:eq(4)').text():null,
                        discount : $(tr).find("input:eq(0)").val()
                    });
                });
                const bet = {
                    bet_id: $("input[name=hidden_id]").val(),
                    delete_id: $("input[name=hidden_id]").attr('data-delete-id'),
                    balance: parseInt($("#divAccount p span").text()),
                    shift: $('select#shift').val(),
                    lottery: $("#lottery").val() == 'undefined'? false:$("#lottery").val(),
                    ticket: $('#ticket').val(),
                    total: $("#total li:first-child span").text(),
                    grand_total: $("#total li:last-child span").text(),
                    bet_data: JSON.stringify(bet_data)
                };

                $.ajax({
                    type:'POST',
                    url:'{{ route('store-bet') }}',
                    data:bet,
                    success:function(data){
                        if(data.closing_time) {
                            $("#homeModal").modal('show');
                            if (data.closing_time == 1) {
                                $("#homeModal .modal-body").html('<p class="text-danger">{{trans("words.messages.day_time_expired")}}</p>');
                            } else {
                                $("#homeModal .modal-body").html('<p class="text-danger">{{trans("words.messages.night_time_expired")}}</p>');
                            }
                            clearAfterBet();
                        }
                        else{
                            if(data.success){
                                $("#homeModal .modal-body").html('<p>'+data.msg+'</p>');
                                $("#homeModal .modal-body").append('<span class="invisible">'+data.success+'</span>');
                                $("#divAccount p span").html((data.updated_balance).toLocaleString());

                                //Clear all data as new ticket and focus on bet number box
                                clearAfterBet();
                                $('#ticket').val(parseInt(data.ticket)+1);
                            }
                            else{
                                $("#homeModal .modal-body").html('<p class="text-danger">'+data.msg+'</p>');
                                $("#homeModal").modal('show');
                            }
                        }
                    }
                });
                e.preventDefault();
            });
            /** New Button **/
            $("#btn-new").click(function(e){
                $("#bet-number").removeAttr("disabled", "disabled");
                clearAfterBet();
                $('#datepicker').datepicker("setDate", "0");
                //window.location.reload();
                e.preventDefault();
            });


            /** Keypad and Input box **/
            //Disabled input on keyboard
            $(this).on("keydown keypress keyup", false);
            // Declare all calculator's key
            const keys = document.querySelectorAll('#calculator span'),
                inputs = document.querySelectorAll('#calculator input'),
                ticket = document.querySelector('#ticket'),
                allButtons = document.querySelectorAll("span.operator"),
                calculateKeys = document.querySelectorAll("#calculator span:not([class])"),
                inputNumber = document.querySelector('#bet-number'),
                textOperator = document.querySelector('#bet-operator'),
                inputType = document.querySelector('#bet-type'),
                inputAmount = document.querySelector('#bet-amount'),
                btnBackspace = document.querySelector('#btn-backspace');
            let typeMaxLength = 1;
            window.onload = function() {
                setFocus([inputNumber]);
            };
            inputs.forEach(function (inputField) {
                inputField.addEventListener("focus", function (e) {
                    setFocus(inputs,false);
                    setFocus([this]);
                    setValidationDisabled(this,allButtons,calculateKeys,inputs,textOperator);
                    setFocus([ticket],false);
                    /*ticket.value='1';*/

                    // prevent page jumps
                    e.preventDefault();
                });
            });
            ticket.addEventListener("focus", function (e) {
                setFocus(inputs,false);
                setFocus([this]);
                if(ticket.value.length >0){
                    setDisabled([btnBackspace],null,false);
                }
                // prevent page jumps
                e.preventDefault();
            });
            // loop through all keys
            for(let i = 0; i < keys.length; i++) {
                //add onclick event to the keys
                keys[i].onclick = function(e) {
                    const inputFocus = document.querySelector('.focus'),
                        btnVal = this.innerHTML;

                    // If clear key is pressed, erase everything
                    if(this.classList.contains('operator')) {
                        switch (this.id) {
                            case 'btn-clear':
                                if(inputFocus.id == 'bet-type'){
                                    clearText([textOperator,inputFocus]);
                                    setDisabled([inputFocus]);
                                    if(inputAmount.value.length > 0){
                                        setFocus([inputAmount]);
                                    }
                                    else {
                                        setFocus([inputNumber]);
                                    }
                                }
                                else{
                                    if(inputFocus.id == 'bet-number'){
                                        clearText([textOperator],inputs);
                                    }
                                    else{
                                        clearText([inputFocus]);
                                    }
                                    setValidationDisabled(inputFocus,allButtons,calculateKeys,inputs,textOperator);
                                }
                                break;
                            case 'btn-backspace':
                                inputFocus.value= inputFocus.value.substr(0, inputFocus.value.length - 1);
                                setValidationDisabled(inputFocus,allButtons,calculateKeys,inputs,textOperator);
                                break;
                            case 'btn-roll-first':
                            case 'btn-roll-middle':
                            case 'btn-roll-last':
                            case 'btn-box':
                                //clearText([inputType]);
                                textOperator.innerHTML= this.attributes.rel.value;
                                textOperator.setAttribute('data-operator',this.getAttribute('data-operator'));
                                setDisabled([inputType],null,false);
                                setFocus([inputType]);
                                setDisabled([btnBackspace]);
                                typeMaxLength=inputNumber.value.length;
                                break;
                            case 'btn-enter':
                                if(inputNumber.value.length > 1){
                                    setFocus([inputAmount]);
                                }
                                break;
                        }
                    }
                    else{
                        inputFocus.value += btnVal;
                        setValidationDisabled(inputFocus,allButtons,calculateKeys,inputs,textOperator,btnVal);
                    }

                    // Set max length of the digits.
                    inputNumber.value = inputNumber.value.substr(0, 4);
                    inputType.value = inputType.value.substr(0, typeMaxLength);
                    inputAmount.value = inputAmount.value.substr(0, 6);

                    // prevent page jumps
                    e.preventDefault();
                }
            }

        });


        /** Clear Data After Bet**/
        function clearAfterBet() {
            $("#bet-list tbody").empty();
            $("#total li span").text('0');
            $("li.discount").remove();
            $("#calculator input").val('');
            $("#bet-number").focus();
            $("#homeModal .modal-body span").remove();
            $("#btn-bet").addClass('isDisabled');
            $("input[name=hidden_id]").val('0');
            $("input[name=hidden_id]").attr('data-delete-id','0');
        }
        /** Delete row table bet **/
        function deleteEntry($this) {
            $($this).parents('tr').remove();
            $("#total li span").text('0');
            $("li.discount").remove();
            $('#bet-list tbody tr').each(function(index){
                /*$(this).attr('data-delete-id',index+1);*/
                setTotal(
                    $(this).find('td:eq(3)').text().replace(/[^0-9\.-]+/g, ""),
                    $(this).find("input:eq(0)").val(),
                    $(this).find("input:eq(1)").val()
                );
            });

            // Get Id of delete rows
            const delete_id = $("input[name=hidden_id]").attr('data-delete-id') == 0 ? [] : [$("input[name=hidden_id]").attr('data-delete-id')];
            delete_id.push($($this).parents('tr').attr('data-detail-id'));
            $("input[name=hidden_id]").attr('data-delete-id',delete_id);

            //
            if($('#bet-list tbody tr').length < 1){
                clearTable();
            }
            else{
                $("#btn-bet").removeClass('isDisabled');
            }
        }
        /** Edit row table bet **/
        function editEntry($this,id) {
            const currentRow = $($this).closest("tr");
            const bet_num = currentRow.find("td:eq(1)").text().split(' ');
            const post = currentRow.find("td:eq(0)").text().split(',');

            $("#bet-operator").text('');
            $('#bet-operator').attr('data-operator','');
            $('#divPost li').removeClass('current');
            $('#bet-list tbody tr a').removeClass('isDisabled');


            if(post[0].toUpperCase() == 'LO'){
                $('#divPost li a[rel='+ (post.length > 3 ? 'LO4P' : 'LOAB') +']').parents('li').addClass('current');
            }
            else{
                if(post.length >= 4){
                    $('#divPost li a[rel=4P]').parents('li').addClass('current');
                }
                post.forEach(function(item) {
                    $('#divPost li a[rel='+item+']').parents('li').addClass('current');
                });
            }

            $("input[name=hidden_id]").attr('data-detail-id', currentRow.data('detail-id'));

            $("input[name=hidden_id]").attr('data-row-id',currentRow.attr('data-row-id'));
            $("#bet-number").val(bet_num[0]);
            $("#bet-operator").text(bet_num[1]);
            $('#bet-operator').attr('data-operator',currentRow.find("td:eq(1)").attr('data-operator'));
            $("#bet-type").val(bet_num[2]);
            $("#bet-amount").val(currentRow.find("td:eq(2)").text().replace(/[^0-9\.-]+/g,""));

            $($this).siblings().addClass('isDisabled');
            $($this).addClass('isDisabled');
            $("#bet-amount").removeAttr("disabled", "disabled");
            $("#bet-amount").focus();
        }
        /** Set Total **/
        function setTotal(sub_total,discount_setting,digit){
            const discount_percentage = 100 - discount_setting;
            const discount = parseInt(sub_total) * discount_percentage / 100;
            const total = parseInt($('#total li:first-child span').text()) + parseInt(sub_total);
            const grand_total = parseInt($('#total li:last-child span').text()) + (parseInt(sub_total) - discount);
            let new_discount = true;

            $("#total li:first-child span").html(total);
            $("#total li:last-child span").html(grand_total);

            /** Display Discount **/
            if($(".discount").length > 0){
                $(".discount").each(function() {
                    if($(this).children("span:nth-child(2)").text() == discount_percentage){
                        $(this).children("span:last-child").html(parseInt($(this).children("span:last-child").text()) + discount);
                        if($(this).children("span:first-child").text() != digit)
                            $(this).children("span:first-child").html('{{trans("words.all")}}');
                        new_discount=false;
                    }
                });
            }
            if(new_discount){
                $("#total li:first-child").after('' +
                    '<li class="discount">{!! trans("words.discount") !!} ' +
                    '<span>'+digit+'</span> ' +
                    '<span>'+discount_percentage+'</span>% : ' +
                    '<span>'+discount+'</span>{{$user->location->currency->symbol}}' +
                    '</li>');
            }
        }
        /** Clear Table **/
        function clearTable() {
            $("#bet-list tbody").empty();
            $("#total li span").text('0');
            $("#calculator input").val('');
            $("#bet-number").focus();
            $("li.discount").remove();
            $("#homeModal .modal-body").children().remove();
        }
        /** Set Disabled or Enabled by parameter **/
        function setDisabled(elements, elements2=null, set=true){
            //If has element2, add element2(attribute) to element array.
            let allElements=elements;
            if(elements2!=null){
                allElements= Array.prototype.slice.call(elements).concat(Array.prototype.slice.call(elements2));
            }

            //Loop all element
            allElements.forEach(function(element) {
                //if is input type set disabled attribute (true or false)
                if(element.tagName == "INPUT"){
                    element.disabled=set;
                }
                //if not input type add or remove isDisabled class
                else{
                    if(set==true){
                        element.classList.add("isDisabled");
                    }
                    else{
                        element.classList.remove("isDisabled");
                    }
                }
            });
        }
        /** Clear text input box or Html tag **/
        function clearText(elements, elements2=null) {
            //If has element2, add element2(attribute) to element array.
            if(elements2!=null){
                elements2.forEach(function (e) {
                    elements.push(e);
                })
            }
            elements.forEach(function (element) {
                if(element.tagName == "INPUT"){
                    element.value= '';
                }
                else{
                    element.innerHTML= '';
                    if(element.hasAttribute('data-operator')){
                        element.setAttribute('data-operator','');
                    }
                }
            });
        }
        /** Set focus input box **/
        function setFocus(elements, set=true) {
            //Loop all element
            elements.forEach(function(element) {
                if(set==true){
                    element.classList.add("focus");
                    element.focus();
                }
                else{
                    element.classList.remove("focus");
                }
            });
        }
        /** Set Input Value **/
        function setInputTypeValue(inputValue, inputNumber, keys, index, btnBox=false, thisKey = '9') {
            if(btnBox){
                inputValue.value= thisKey === '9' ? inputNumber.value.length : thisKey;
                keys.forEach(function (key) {
                    if(key.innerHTML > inputNumber.value.length || key.innerHTML<= 1){
                        setDisabled([key]);
                    }
                    else{
                        setDisabled([key],null,false);
                    }
                });
            }
            else{
                inputValue.value=inputNumber.value.substr(0, index) + thisKey + inputNumber.value.substr(index + 1);
                keys.forEach(function (key) {
                    if(key.innerHTML <= inputNumber.value.charAt(index)){
                        setDisabled([key]);
                    }
                    else{
                        setDisabled([key],null,false);
                    }
                });
            }
            inputValue.setSelectionRange(index,index+1);
        }
        /** Set element Validation by Disabled **/
        function setValidationDisabled(thisInput,allButton,calculateKeys,input,operator,thisKey = '9'){
            let btnBet = false;
            if(thisInput.id == 'bet-number'){
                if(input.item(0).value.length != input.item(1).value.length){
                    clearText([input.item(1),operator]);
                    setDisabled([input.item(1)]);
                }
                switch (thisInput.value.length) {
                    case 0:
                        setDisabled([input.item(1), input.item(2)], allButton);
                        setDisabled(calculateKeys, null, false);
                        if (document.querySelectorAll('#betTable tbody tr').length > 0) {
                            btnBet = true;
                        }
                        break;
                    case 1:
                        setDisabled(allButton, [input.item(2)]);
                        setDisabled([allButton.item(1), allButton.item(2)], calculateKeys, false);
                        break;
                    case 3:
                        setDisabled(allButton, calculateKeys, false);
                        break;
                    case 2:
                    case 4:
                        setDisabled(calculateKeys, allButton, false);
                        setDisabled([input.item(2)], null, false);
                        setDisabled([allButton.item(3)]);
                        break;
                    default:
                        thisInput.style.borderColor = "red";
                        setTimeout(function () {
                            thisInput.style.borderColor = "#5d7bba";
                        }, 100);
                        break;
                }
                if (document.querySelector("input[name=hidden_id]").dataset.rowId > 0){
                    setDisabled([allButton.item(1),allButton.item(2)]);
                    if(thisInput.value.length > 2){
                        setDisabled([allButton.item(2)],null,false);
                    }
                }
            }
            else if(thisInput.id == "bet-type"){
                let index=0, box=false;
                switch(operator.getAttribute('data-operator')){
                    case "box":
                        box = true;
                        break;
                    case "roll-last":
                        index = input.item(0).value.length-1;
                        break;
                    case "roll-middle":
                        index = 1;
                        break;
                }
                setInputTypeValue(thisInput, input.item(0),calculateKeys,index,box,thisKey);
                setDisabled([allButton.item(1),allButton.item(7)],null,false);
            }
            else{
                setDisabled(calculateKeys,null,false);
                if(thisInput.value.length < 1){
                    //Disable button 0, clear, backspace and enter if bet amount empty
                    setDisabled([calculateKeys.item(9),allButton.item(1),allButton.item(2),allButton.item(7)]);
                }
                else{
                    if(thisInput.id == "bet-amount"){
                        setDisabled([input.item(1)],null, input.item(1).value.length > 0 ? false:true);
                        setDisabled(allButton, input.item(1).value.length > 0 ? [input.item(1)]:null, false);
                    }
                    else{
                        setDisabled(allButton);
                        setDisabled(calculateKeys, [allButton.item(2), allButton.item(7)], false);
                    }
                }
            }
            setDisabled([allButton.item(0),allButton.item(8)]);
            if(btnBet==true){
                setDisabled([allButton.item(8)],null,false);
            }
        }
        /** Ajax Ticket **/
        function ajaxTicket(today){
            const shift = $("#shift").val();
            const date = $("#datepicker").val();
            const lottery = $("#lottery").val();
            const selectedDate = new Date(date);
            $.ajax({
                type: 'GET',
                url: '{{ route('ajax-ticket') }}',
                data: {shift: shift, date: date, lottery: lottery},
                success: function (data) {
                    const newTicket = (today.getTime() == selectedDate.getTime() || parseInt(data)==0) ? 1 : 0;
                    $('#ticket').val(parseInt(data)+newTicket);
                    if($('#lottery').val()=='2'){
                        $('#shift').prop("selectedIndex", 0);
                    }

                    loadData();
                }
            });
        }

        function ajaxPost() {
            $.ajax({
                type: 'GET',
                url: '{{ route('ajax-post') }}',
                data: {lottery: lottery},
                success: function (data) {
                    let posts = '';
                    $.each(data, function(key, value) {
                        posts+=
                            '<li>' +
                            '<div class="outer circle shapeborder">' +
                            '<div class="inner circle shapeborder">' +
                            '<a href="#" rel="'+value.post+'">'+value.post+'</a>' +
                            '</div>' +
                            '</div>' +
                            '</li>';
                    });
                    $('#divPost ul').html(posts);
                    $('#divPost li:first-child').addClass('current');
                }
            });
        }
        /** Data on change**/
        function loadData(){
            const shift = $("#shift").val();
            const date = $("#datepicker").val();
            const ticket = $("#ticket").val();
            const lottery = $("#lottery").val() == 'undefined'? false:$("#lottery").val();
            $.ajax({
                type: 'GET',
                url: '{{ route('ajax-onChange') }}',
                data: {shift: shift, date: date, ticket: ticket, lottery: lottery},
                success: function (data) {
                    $("input[name=hidden_id]").val(data.bet_id);
                    $("#bet-list tbody").empty();
                    $("#total li span").text('0');
                    $("li.discount").remove();

                    //Clear all input box and focus on bet number box
                    $("#calculator input").val('');
                    $("#bet-operator").text('');
                    $('#bet-operator').attr('data-operator','');
                    $("#bet-type").attr('disabled', 'disabled');
                    $("span.operator").addClass('isDisabled');
                    $("#btn-bet").addClass('isDisabled');

                    const today = new Date(), selected_date = new Date($("#datepicker").val());
                    if (data.success) {
                        $.each(data.bet_list, function (index) {
                            $("#bet-list tbody").append(data.bet_list[index]);
                            setTotal(data.sub_total[index], data.setting[index]['discount'], data.setting[index]['digit']);
                            $("input[name=hidden_id]").attr('data-row-id', '0');
                            $("input[name=hidden_id]").attr('data-detail-id', '0');
                            $("input[name=hidden_id]").attr('data-delete-id', '0');
                        });
                    }

                    if (today.toDateString() == selected_date.toDateString()) {
                        if(!$("#ticket").hasClass('focus')) {
                            $("#bet-list tbody td a, #calculator span, #divPost li").removeClass('isDisabled');
                            $("#bet-number").removeAttr("disabled", "disabled");
                            $("#bet-number").focus();
                        }
                        if(data.over_time){
                            $("#calculator input").attr('disabled', 'disabled');
                            $("#bet-list tbody td a, #calculator span, #divPost li").addClass('isDisabled');
                            $("#btn-new").removeClass('isDisabled');
                        }
                    }
                    else{
                        /*$("#bet-list tbody").html('<td colspan="6" class="text-center">No Data on this day!</td>');*/
                        $("#calculator input").attr('disabled', 'disabled');
                        $("#bet-list tbody td a, #calculator span, #divPost li").addClass('isDisabled');
                        $("#btn-new").removeClass('isDisabled');
                    }

                    //Set Ticket Number
                    if(data.ticket){
                        $('#ticket').val(data.ticket);
                    }
                }
            });
        }

    </script>
@endsection
