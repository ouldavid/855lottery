<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/assets/dist/img/user2-160x160.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><a href="{{ route('profile') }}">{{ Auth::user()->name }}</a></p>
                {{--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>--}}
                <small><small><a href="{{ route('profile') }}"><span><i class="fa far fa-user-circle"></i> My Account</span></a> &nbsp;  &nbsp;
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        <i class="fas fa-sign-out-alt"></i> <span>{{ __('Logout') }}</span>
                    </a>
                </small></small>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">

            <!-- Main Manu -->
            <li class="header">MAIN NAVIGATION</li>
            @if(auth()->user()->hasAnyRole(['Admin','Master','Account']))
                <li><a href="{{ route('dashboard') }}"><i class="fas fa-tachometer-alt"></i> <span>Dashboard</span></a></li>
            @endif
            <li><a href="{{ route('bet') }}"><i class="fas fa-coins"></i> <span>Bet</span></a></li>
            <li><a href="{{ route('winner') }}"><i class="fas fa-hand-holding-usd"></i> <span>Winner</span></a></li>
            <li><a href="{{ route('result') }}"><i class="fas fa-eye"></i> <span>Result Release</span></a></li>
            @if(auth()->user()->hasAnyRole(['Admin','Master','Account']))
                <li><a href="{{ route('user') }}"><i class="fa fa-user"></i> Users</a></li>
            @endif

            <!-- Report -->
            <li class="header">REPORT</li>
            <li><a href="{{ route('profit') }}"><i class="fas fa-file-invoice-dollar"></i> Profit & Lost</a></li>
            <li><a href="{{ route('transaction') }}"><i class="fas fa-exchange-alt"></i> Transaction</a></li>

            <!-- Setting -->
            <li class="header">SETTINGS</li>
            <li><a href="{{ route('setting') }}"><i class="fas fa-cogs"></i> Setting</a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
